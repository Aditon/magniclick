"use strict;"
$(document).ready( function() {


  var $body = $(document.body),
      $window = $(window),
      $navBar = $('.js-topBar').length ? $('.js-topBar') : null,
      $openSearchInput = $('.js-topBarSearch').length ? $('.js-topBarSearch') : null,
      $navToggle = $('.js-navToggle').length ? $('.js-navToggle') : null,
      $mobileMenu = $('.js-mobileMenu').length ? $('.js-mobileMenu') : null,
      $specialCards = $('.js-specialCards').length ? $('.js-specialCards') : null,
      $productsGrid = $('.js-msGrid').length ? $('.js-msGrid') : null,
      $socialWidgetSlider = $('.js-socialWidgetSlider').length ? $('.js-socialWidgetSlider') : null,
      $numberControl = $('.js-numberControl').length ? $('.js-numberControl') : null,
      $productJumbSlider = $('.js-productJumbSlider').length ? $('.js-productJumbSlider') : null,
      $productBoarThumb = $('.js-productBoarThumb li').length ? $('.js-productBoarThumb li') : null,
      $openContacts = $('.js-openContacts').length ? $('.js-openContacts') : null,
      $contacts = $('.js-contactsPopup').length ? $('.js-contactsPopup') : null,
      $closeContacts = $('.js-closePopup').length ? $('.js-closePopup') : null,
      $closeCartItem = $('.js-closeCartItem').length ? $('.js-closeCartItem') : null,
      $privatePolicy = $('.private-policy').length ? $('.private-policy') : null,
      $closeCartPopup = $('.js-closeCartPopup').length ? $('.js-closeCartPopup') : null,
      sliderNavText = ['',''],
      sliderSmarSpeed = 500;

  // Private Policy Unlock Btn
  if( $privatePolicy ) {
    var $checkInput = $privatePolicy.find( 'input[type="checkbox"]' ),
        $formBtn = $privatePolicy.parents( 'form' ).find( 'button' );
        $formBtn.prop( 'disabled', true );

    $checkInput.on( 'change', function() {
      var $thisFormBtn = $(this).parents( 'form' ).find( 'button' );

      if( $(this).is(':checked') ) {
        $thisFormBtn.prop( 'disabled', false );
      }else {
        $thisFormBtn.prop( 'disabled', true );
      }
    } );
  }

  // Open Top Bar Search
  if( $openSearchInput ) {
    var inputClassName = 'open-search-input',
        $elemParent = $openSearchInput.parent(),
        timer;

    $openSearchInput.on( 'mouseover', function( e ) {
      clearTimeout( timer );

      $elemParent.addClass( inputClassName );

        $(document).on( 'click', function( e ) {

          if( $elemParent.hasClass( inputClassName ) ){
            if( $elemParent.has(e.target).length === 0 ) {
              $elemParent.removeClass( inputClassName );
            }
          }else {
            $(this).unbind( 'click' );
          }

        } );

    });

    $elemParent.on( 'mouseleave', function() {

      if( !$(this).find( 'input' ).is( ':focus' ) ) {
        timer = setTimeout( function() {
          $elemParent.removeClass( inputClassName );
        }, 1500 );
      }

    } );

  } 

  // Tob bar fixed
  if( $navBar ) {
    var $menuLineTopOffset = 660,
        $menuHeight = $navBar.parent().height();

    function fixedTopBar( $elem ) {
      var fixedClass = 'fixed';

      if( $( $elem ).scrollTop() >= $menuLineTopOffset ) {
        $('.container-fluid').addClass( fixedClass ).css( 'padding-top', $menuHeight );
      }else {
        $('.container-fluid').removeClass( fixedClass ).css( 'padding-top', 0 );
      }
    }

    $window.on( 'scroll', function( e ) {
      fixedTopBar( $window );
    } );

    fixedTopBar( $window );

  }

  // Navigation Toggle
  if( $navToggle ) {

    $navToggle.on( 'click', function(e) {
      e.preventDefault();
      $body.toggleClass( 'open-nav-menu' );
    } );
  }

  // Mobile Menu
  if( $mobileMenu ) {

    $mobileMenu.on( 'click', function(e) {
      var $target = $(e.target);

      if( $target.hasClass( 'mobile-menu-arrow-wrapper' ) || $target.hasClass( 'icon' ) ) {
        console.log( 1 );
        e.preventDefault();
        $target.parents('li').toggleClass( 'open-sub-menu' );
      }
    } );
  }

  // Special Cards Slider
  if( $specialCards ) {

    function checkWidthForSpecialCarousel() {
      if( $(window).width() >= 768 ) {
        $specialCards.trigger('destroy.owl.carousel');
      }else {
        $specialCards.owlCarousel({
          items: 5,
          smartSpeed: sliderSmarSpeed,
          nav: true,
          navText: sliderNavText,
          dots: false,
          responsive: {
            0: {
              items: 1,
              loop: true
            },
            480: {
              items: 2,
              loop: true
            },
            768: {
              loop: false,
              nav: false
            }
          }
        });
      }
    }

    var $specialCardsNavContainer = $( '.js-specialArrows' ).length ? $('.js-specialArrows') : null;

    if( $specialCardsNavContainer ) {
      var $sliderSpecialCardsPrev = $specialCardsNavContainer.find( '.prev' ),
          $sliderSpecialCardsNext = $specialCardsNavContainer.find( '.next' );

      $sliderSpecialCardsNext.click(function() {
        $specialCards.trigger('next.owl.carousel');
      });

      $sliderSpecialCardsPrev.click(function() {
        $specialCards.trigger('prev.owl.carousel');
      });
    }

    $(window).resize(function() {
      checkWidthForSpecialCarousel();
    });

    checkWidthForSpecialCarousel();

  }

  // Productst Grid
  if( $productsGrid ) {

    $productsGrid.masonry({
      itemSelector: '.grid-item',
      gutter: 24,
    });

  }

  // Social Widget Slider
  if( $socialWidgetSlider ) {
    $socialWidgetSlider.owlCarousel({
      items: 4,
      smartSpeed: sliderSmarSpeed,
      loop: true,
      nav: true,
      navText: sliderNavText,
      dots: false,
      responsive: {
        0: {
          items: 1,
        },
        480: {
          items: 2
        },
        767: {
          items: 3
        },
        1140: {
          items: 4
        }
      }
    });

    var $sliderNavContainer = $( '.js-socialWidgetNav' ).length ? $('.js-socialWidgetNav') : null;

    if( $sliderNavContainer ) {
      var $sliderPrev = $sliderNavContainer.find( '.prev' ),
          $sliderNext = $sliderNavContainer.find( '.next' );

      $sliderNext.click(function() {
        $socialWidgetSlider.trigger('next.owl.carousel');
      });

      $sliderPrev.click(function() {
        $socialWidgetSlider.trigger('prev.owl.carousel');
      });
    }

  }

  // Product Jumb Slider
  if( $productJumbSlider ) {

    $productJumbSlider.owlCarousel({
      items:1,
      autoplay: true,
      smartSpeed: sliderSmarSpeed,
      nav:true,
      navText: sliderNavText,
      dots: false,
      loop : true,
      mouseDrag: false,
      touchDrag: false,
      thumbs: true,
      thumbsPrerendered: true,
      thumbContainerClass: 'js-productJumbThumb',
      thumbItemClass: 'item',
      onInitialized: getParentSize,
      onResize: getParentSize
    });

    function getParentSize() {
      var $parentHeight = $productJumbSlider.parent().height(),
          $elemChildren = $productJumbSlider.find( '.item' );
          $elemChildren.css({'height': $parentHeight});
    }

    var $jumbArrowsContainer = $('.js-jumbArrows').length ? $('.js-jumbArrows') : null;

    if( $jumbArrowsContainer ) {
      var $jumbSliderPrev = $jumbArrowsContainer.find( '.prev' ),
          $jumbSliderNext = $jumbArrowsContainer.find( '.next' );

      $jumbSliderNext.click(function() {
        $productJumbSlider.trigger('next.owl.carousel');
      });

      $jumbSliderPrev.click(function() {
        $productJumbSlider.trigger('prev.owl.carousel');
      });
    }
  }

  // Number Control
  if( $numberControl ) {

    $numberControl.each(function() {
      var $plus = $(this).find( '.plus' ),
          $minus = $(this).find( '.minus' ),
          $total = $(this).find( '.total' ),
          $parent = $(this),
          $board = $(this).parents( '.product-board' ),
          $totalCost = Number( $board.data('total-cost') ),
          $totalCostContainer = $board.find( '.product-cost .total' );

      $plus.on( 'click', function(e) {
        var $totalSum = Number($total.val()) + 1,
            $currentTotal = Number( $totalCostContainer.text() ),
            $dataTotal = $currentTotal + $totalCost;
        $total.val( $totalSum );
        $parent.attr( 'data-total', $totalSum);
        $totalCostContainer.text( $dataTotal );
      } );

      $minus.on( 'click', function(e) {
        var $totalSum = Number($total.val()) - 1,
            $currentTotal = Number( $totalCostContainer.text() ),
            $resultTotal = $currentTotal - $totalCost;
        if( Number($total.val()) <= 1 ) {
          return;
        }else {
          $parent.attr( 'data-total', $totalSum);
          $total.val( $totalSum );
          $totalCostContainer.text( $resultTotal  );
        }
      } );

      $total.on( 'keypress', function( e ) {
        if( e.keyCode == 13 ){
          e.preventDefault();
          var $currentTotal = Number( $totalCostContainer.text() ),
              $allElemsCount = 0;
          $parent.attr( 'data-total', $(this).val() );

          $('.product-board [data-total]').each( function() {
            $allElemsCount += Number( $(this).data( 'total' ) );
          } );

          $totalCostContainer.text( $allElemsCount * $totalCost );

        }
      } );

      $total.on('input', function() {
        $(this).val($(this).val().replace(/[A-Za-zА-Яа-яЁё]/, ''));
      })

      $total.on( 'blur', function() {
        if( $(this).val() == '' ) {
          $(this).val( 1 );
          $parent.attr( 'data-total', 1 );
        }
      } );
    } );


  }

  if( $productBoarThumb ) {
    var className = 'active';

    $productBoarThumb.on( 'click', function(e) {
      var $elemParent = $(this).parent(),
          $elemData = $(this).data( 'props' );

      $(this).toggleClass( className ).siblings().removeClass( className );

      if( $(this).hasClass( className ) ) {
        $elemParent.attr( 'data-total', $elemData );
      }else {
        $elemParent.attr( 'data-total', '' );
      }

    } );

    $productBoarThumb.on( 'keypress', function(e) {

      if( e.keyCode == 13 ){
        e.preventDefault();
        $(this).toggleClass( className ).siblings().removeClass( className );
      }
    } )
  }

  // Open Contacts
  if( $openContacts ) {
    var openContactsClass = 'open-contact-popup';

    $openContacts.on( 'click', function(e) {
      e.preventDefault();

      // If Contacts Form
      if( $contacts ) {
        $body.addClass( openContactsClass );
      }
    } );

    // If Close Form
    if( $closeContacts ) {

      $closeContacts.on( 'click', function() {
        $body.removeClass( openContactsClass );
      } );
    }
  }

  if( $closeCartItem ) {

    $closeCartItem.on( 'click', function(e) {
      e.preventDefault();

      $(this).parents( '.item' ).remove();
    } );
  }

  if( $closeCartPopup ) {

    $closeCartPopup.on( 'click', function( e ) {
      e.preventDefault();
      $(this).parents( '.cart-popup' ).hide();
    } );
  }

} );
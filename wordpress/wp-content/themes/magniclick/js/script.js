"use strict;"
$(document).ready( function() {


  var $body = $(document.body),
      $window = $(window),
      $navBar = $('.js-topBar').length ? $('.js-topBar') : null,
      $openSearchInput = $('.js-topBarSearch').length ? $('.js-topBarSearch') : null,
      $navToggle = $('.js-navToggle').length ? $('.js-navToggle') : null,
      $mobileMenu = $('.js-mobileMenu').length ? $('.js-mobileMenu') : null,
      $specialCards = $('.js-specialCards').length ? $('.js-specialCards') : null,
      $instagram_widget = $('.jr-insta-thumb #wis-slides').length ? $('.jr-insta-thumb #wis-slides') : null,
      $productsGrid = $('.js-msGrid').length ? $('.js-msGrid') : null,
      $socialWidgetSlider = $('.js-socialWidgetSlider').length ? $('.js-socialWidgetSlider') : null,
      $numberControl = $('.js-numberControl').length ? $('.js-numberControl') : null,
      $productJumbSlider = $('.js-productJumbSlider').length ? $('.js-productJumbSlider') : null,
      $productBoarThumb = $('.js-productBoarThumb li').length ? $('.js-productBoarThumb li') : null,
      $openContacts = $('.js-openContacts').length ? $('.js-openContacts') : null,
      $contacts = $('.js-contactsPopup').length ? $('.js-contactsPopup') : null,
      $closeContacts = $('.js-closePopup').length ? $('.js-closePopup') : null,
      $privatePolicy = $('.private-policy').length ? $('.private-policy') : null,
      $closeCartPopup = $('.js-closeCartPopup').length ? $('.js-closeCartPopup') : null,
      colorPickerProductClass = '.js-colorPickerProduct',
      $colorWrappers = $(colorPickerProductClass + ' input.total').length ? $(colorPickerProductClass + ' input.total') : null,
      $faqLine = $('.js-faqLine').length ? $('.js-faqLine') : null,
      $cartBtn = $('.js-cartBtn').length ? $('.js-cartBtn') : null,
      $mainCartBtn = $('.js-mainCartBtn').length ? $('.js-mainCartBtn') : null,
      $arrowUp = $('.js-arrowUp').length ? $('.js-arrowUp') : null,
      currentPriceAttr = 'data-current-price',
      totalCostAttr = 'data-total-cost',
      sliderNavText = ['',''],
      sliderSmarSpeed = 500;

  // Private Policy Unlock Btn
  if( $privatePolicy ) {
    var $checkInput = $privatePolicy.find( 'input[type="checkbox"]' ),
        $formBtn = $privatePolicy.parents( 'form' ).find( 'button' );
        $formBtn.prop( 'disabled', true );

    $checkInput.on( 'change', function() {
      var $thisFormBtn = $(this).parents( 'form' ).find( 'button' );

      if( $(this).is(':checked') ) {
        $thisFormBtn.prop( 'disabled', false );
      }else {
        $thisFormBtn.prop( 'disabled', true );
      }
    } );
  }

  // Open Top Bar Search
  if( $openSearchInput ) {
    var inputClassName = 'open-search-input',
        $elemParent = $openSearchInput.parent(),
        timer;

    $openSearchInput.each( function() {

      $(this).on( 'mouseover', function( e ) {
        clearTimeout( timer );

        var $elemParent = $(this).parent();

        $elemParent.addClass( inputClassName );

          $(document).on( 'click', function( e ) {

            if( $elemParent.hasClass( inputClassName ) ){
              if( $elemParent.has(e.target).length === 0 ) {
                $elemParent.removeClass( inputClassName );
              }
            }else {
              $(this).unbind( 'click' );
            }

          } );

      });

    } );

    $elemParent.on( 'mouseleave', function() {

      if( !$(this).find( 'input' ).is( ':focus' ) ) {
        timer = setTimeout( function() {
          $elemParent.removeClass( inputClassName );
        }, 1500 );
      }

    } );

  } 

  // Tob bar fixed
  if( $navBar ) {
    var $menuLineTopOffset = 660,
        $menuHeight = $navBar.parent().height();

    function fixedTopBar( $elem ) {
      var fixedClass = 'fixed';

      if( $( $elem ).scrollTop() >= $menuLineTopOffset ) {
        $('.container-fluid').addClass( fixedClass ).css( 'padding-top', $menuHeight );
      }else {
        $('.container-fluid').removeClass( fixedClass ).css( 'padding-top', 0 );
      }
    }

    $window.on( 'scroll', function( e ) {
      fixedTopBar( $window );
    } );

    fixedTopBar( $window );

  }

  // Arrow Slide UP
  if( $arrowUp ) {
    $arrowUp.on('click', function(e) {
      e.preventDefault();
      $('html, body').animate({scrollTop: 0}, 400)
    });
  }

  // Navigation Toggle
  if( $navToggle ) {

    $navToggle.on( 'click', function(e) {
      e.preventDefault();
      $body.toggleClass( 'open-nav-menu' );
    } );
  }

  // Mobile Menu
  if( $mobileMenu ) {

    $mobileMenu.on( 'click', function(e) {
      var $target = $(e.target);

      if( $target.hasClass('mobile-menu-arrow-wrapper') || $target.hasClass( 'icon' ) || $target.parent('li').hasClass('menu-item-has-children') ) {
        e.preventDefault();
        $target.parents('li').toggleClass( 'open-sub-menu' );
      }
    } );
  }

  // Productst Grid
  if( $productsGrid ) {

    function setGrid() {
      $productsGrid.masonry({
        itemSelector: '.grid-item',
        gutter: 24,
      });
    }

    setTimeout(function() {
      setGrid();
    }, 0);

    function destroGrid() {
      setTimeout(function() {
        destroGrid();
        setGrid();
      }, 0);
    }


    var $width = $(window).width();
    $(window).on('resize', function(e) {

      if($(this).width() != $width){
        $width = $(this).width();
        destroGrid();
      }
    });

  }

  // Special Cards Slider
  if( $specialCards ) {

    function checkWidthForSpecialCarousel() {
      if( $(window).width() >= 768 ) {
        $specialCards.trigger('destroy.owl.carousel');
      }else {
        $specialCards.owlCarousel({
          items: 5,
          smartSpeed: sliderSmarSpeed,
          nav: true,
          navText: sliderNavText,
          dots: false,
          responsive: {
            0: {
              items: 1,
              loop: true
            },
            480: {
              items: 2,
              loop: true
            },
            768: {
              items: 5,
              loop: false,
              nav: false
            }
          }
        });
      }
    }

    var $specialCardsNavContainer = $( '.js-specialArrows' ).length ? $('.js-specialArrows') : null;

    if( $specialCardsNavContainer ) {
      var $sliderSpecialCardsPrev = $specialCardsNavContainer.find( '.prev' ),
          $sliderSpecialCardsNext = $specialCardsNavContainer.find( '.next' );

      $sliderSpecialCardsNext.click(function() {
        $specialCards.trigger('next.owl.carousel');
      });

      $sliderSpecialCardsPrev.click(function() {
        $specialCards.trigger('prev.owl.carousel');
      });
    }

    $(window).resize(function() {
      checkWidthForSpecialCarousel();
    });

    checkWidthForSpecialCarousel();

  }

  // Social Widget Slider
  if( $instagram_widget ) {

    $instagram_widget.addClass('owl-carousel');

    $instagram_widget.owlCarousel({
      items: 4,
      smartSpeed: sliderSmarSpeed,
      loop: true,
      nav: true,
      navText: sliderNavText,
      dots: false,
      responsive: {
        0: {
          items: 1,
        },
        480: {
          items: 2
        },
        767: {
          items: 3
        },
        1140: {
          items: 4
        }
      }
    });

    var $sliderNavContainer = $( '.js-socialWidgetNav' ).length ? $('.js-socialWidgetNav') : null;

    if( $sliderNavContainer ) {
      var $sliderPrev = $sliderNavContainer.find( '.prev' ),
          $sliderNext = $sliderNavContainer.find( '.next' );

      $sliderNext.click(function() {
        $instagram_widget.trigger('next.owl.carousel');
      });

      $sliderPrev.click(function() {
        $instagram_widget.trigger('prev.owl.carousel');
      });
    }

  }

  // Product Jumb Slider
  if( $productJumbSlider ) {

    $productJumbSlider.owlCarousel({
      items:1,
      autoplay: true,
      smartSpeed: sliderSmarSpeed,
      nav:true,
      navText: sliderNavText,
      dots: false,
      loop : true,
      mouseDrag: false,
      touchDrag: false,
      thumbs: true,
      thumbsPrerendered: true,
      thumbContainerClass: 'js-productJumbThumb',
      thumbItemClass: 'item',
      onInitialized: getParentSize,
      onResize: getParentSize
    });

    function getParentSize() {
      var $parentHeight = $productJumbSlider.parent().height(),
          $elemChildren = $productJumbSlider.find( '.item' );
          $elemChildren.css({'height': $parentHeight});
    }

    var $jumbArrowsContainer = $('.js-jumbArrows').length ? $('.js-jumbArrows') : null;

    if( $jumbArrowsContainer ) {
      var $jumbSliderPrev = $jumbArrowsContainer.find( '.prev' ),
          $jumbSliderNext = $jumbArrowsContainer.find( '.next' );

      $jumbSliderNext.click(function() {
        $productJumbSlider.trigger('next.owl.carousel');
      });

      $jumbSliderPrev.click(function() {
        $productJumbSlider.trigger('prev.owl.carousel');
      });
    }
  }

  // Number Control

    // Get current price
    function getProductCurrentPrice() {
      var productCurrentPrice = document.querySelector( '[' + currentPriceAttr + ']') ? document.querySelector( '[' + currentPriceAttr + ']') : null,
          total = productCurrentPrice ? Number( productCurrentPrice.dataset.currentPrice ) : '' ;
      return total;
    }

    // Check Input Value
    function checkInputValue( elem, type ) {
      var val = (type && type == 'minus') ? 1 : 0;

      if( !elem.parents('.marker-tablets').length ) {

        if( elem.val() <= val && elem.val() != 1 ) {
          elem.parents('.control-wrapper').addClass('empty');
        }else {
          elem.parents('.control-wrapper').removeClass('empty');
        }
      }
    }

    if( $colorWrappers ) {

      $colorWrappers.each( function() {

        checkInputValue( $(this) );

      } );
    }


    $('body').on( 'click', function(e) {
      var $target = $(e.target);

      if( $target.hasClass('plus') ) {
        var $total = $target.parents('.js-numberControl').find( 'input.total' ),
            eventName = 'plus',
            $totalSum = Number($total.val()) + 1;

        if( $colorWrappers ) {
          if( !checkMaxValue( $('.active input.total') ) ) {
            return;
          }
        }

        $total.val( $totalSum ).change();

        if( !$target.parents(colorPickerProductClass).length ) {
          setCurrentPrice( eventName, $totalSum, $target );
        }

        checkInputValue( $total );

        checkCartParent( $target, eventName );

      }else if( $target.hasClass('minus') ) {

        var $total = $target.parents('.js-numberControl').find( 'input.total' ),
            eventName = 'minus';

        if( Number($total.val()) == 0 ) {
          return;
        }else {
          var $totalSum = Number($total.val()) - 1;

          if( Number($total.val()) <= 1 ) {
            checkMinValue( $total,  $totalSum );
          }else {
            $total.val( $totalSum ).change();

            if( !$target.parents(colorPickerProductClass).length ) {
              setCurrentPrice( eventName, $totalSum, $target );
            }
          }
        }

        checkInputValue( $total, eventName );

        checkCartParent( $target, eventName );

      }else if( $target.closest('.js-closeCartItem').length ) {

        e.preventDefault();
        var $elem = $target.closest('.js-closeCartItem'),
            $cookie = checkCookie();

        if( $cookie ) {
          var $elemParent = $elem.parents('.js-cartListItem'),
              $elemParentId = $elemParent.data('object-id'),
              $elemParentKey = $elemParent.data('object-key');
          $cookie = JSON.parse( $cookie );


          // If is cookie, delete this object from cookie and add to
          if( $cookie[$elemParentId][$elemParentKey] ) {
            $cookie.cartTotal -= $cookie[$elemParentId][$elemParentKey].orderQuantity;
            delete $cookie[$elemParentId][$elemParentKey];

            setCookie( $cookie );

            $elem.parents( '.item' ).remove();

            updateCart();
            updateCartTotal( Number($elemParent.find('[data-temp-cost]')[0].dataset.tempCost), 'minus' );

            checkLastCartElem( $('.js-closeCartItem') );
          }
        }

      }

    } );

    function inputEvents() {

      $('input.total').on( 'keypress', function( e ) {
        if( e.keyCode == 13 ){
          if( !$(this).parents(colorPickerProductClass).length ) {
            setCurrentPrice( 'plus', checkTotalSum(), $(this) );
          }
          if( $colorWrappers ) {
            if( !checkMaxValue( $('.active input.total') ) ) {
              checkInputValue( $(this) );
              return;
            }
          }
          checkInputValue( $(this) );
          checkCartParent( $(this) );
        }
      } );

      $('input.total').on('input', function() {
        $(this).val($(this).val().replace(/[A-Za-zА-Яа-яЁё,-]/, ''));
      })

      $('input.total').on( 'blur', function() {
        var $elem = $(this);
        if( $elem.val() == '' ) {
          $elem.val( 0 );
          if( !$(this).parents(colorPickerProductClass).length ) {
            setCurrentPrice( 'plus', checkTotalSum(), $elem );
          }
          checkInputValue( $elem );
        }else {
          if( $colorWrappers ) {
            if( !checkMaxValue( $('.active input.total') ) ) {
              $elem.val( $elem.val() || 0 );
              checkInputValue( $elem );
              return;
            }
          }

          if( !$(this).parents(colorPickerProductClass).length ) {
            setCurrentPrice( 'plus', checkTotalSum(), $elem );
          }
        }

        checkCartParent( $elem, 'blur' );
      } );
    }
    inputEvents();

    var coverPageTotalSum = 0;
    // NUMBER CONTROL FUNCTIONS
    function setCurrentPrice( action, total, elem ) {

      var $productBoardCost = $( '[' + totalCostAttr + ']' ),
          current = document.querySelector( '[' + totalCostAttr + ']' ) ? Number(document.querySelector( '[' + totalCostAttr + ']' ).dataset.totalCost) : '',      

      result = {
        plus: getProductCurrentPrice() * checkTotalSum(),
        minus: Number( current ) - getProductCurrentPrice(),
      },
      price = result[action];

      if( $('[data-quantity]') ) {
        $('[data-quantity]').attr( 'data-quantity', checkTotalSum() );
      }
      if( $('[data-module-quantity]') ) {
        $('[data-module-quantity]').attr( 'data-module-quantity', checkTotalSum() );
      }

      if( !setCoverModuleSum( elem )) {
        $productBoardCost.attr( 'data-total-cost', price );
      }

    }

    // COVER PAGE FUNCTIONS
    function setCoverModuleSum( $elem ) {
      if( checkCoverParent( $elem ) ) {
        var $elemVal = $elem.parent().find('input.total').val(),
            $elemPrice = Number($elem.parents('[data-current-price]').data('current-price')),
            elemSum = 0,
            currentPriceElem = document.querySelector('[data-total-cost]'),
            currentPriceTotal = Number(currentPriceElem.dataset.totalCost);


        $elem.parent().attr('data-temp-total', $elemVal);

        elemSum = $elemPrice * $elemVal;

        var currentTotalSum = 0;
        $('[data-current-price]').each( function() {
          var $elemData = $(this).data('current-price'),
              $elemCount = Number($(this).find('.js-numberControl')[0].dataset.tempTotal);
              currentTotalSum += ( $elemData * $elemCount );

        } );
        
        coverPageTotalSum = currentTotalSum;

        $('[data-total-cost]').attr( 'data-total-cost', coverPageTotalSum );

        return true;
      }
      return false;
    }

    function checkCoverParent( $elem ) {
      if( $elem.parents('.js-moduleBoard').length ) {
        return true;
      }
      return false;
    }

    // Minimum Input Value Watcher
    function checkMinValue( elem, result ) {
      var elemsCount = 0,
          currentElems = 0;
      $( '.active input.total' ).each( function() {
        elemsCount++;
        if( $(this).val() == 0 ) {
          currentElems++;
        }
      } );

      if( (elemsCount - currentElems) <= 1 ) {
        return;
      }else {
          elem.val( result );
          if( !elem.parents(colorPickerProductClass).length ) {
            setCurrentPrice( 'minus', getProductCurrentPrice(), elem );
          }
      }
    }

    // Maximum Input Value Watcher
    function checkMaxValue( elem ) {
      var totalPreset = document.querySelector('[data-current-set]') ? Number(document.querySelector('[data-current-set]').dataset.currentSet) : '',
          elemsValue = 0;

      elem.each( function() {
        elemsValue += Number($(this).val());
      } );

      if( elemsValue >= totalPreset) {
        return false;
      }else {
        return true;
      }
    }

    // Check Total Sum
    function checkTotalSum() {
      var $total = 0;
      $('.js-productModuleBoard input.total').each( function() {
        $total += Number($(this).val());
      } );

      return $total;
    }


  // End Number Control
  if( $productBoarThumb ) {
    var className = 'active';

    $productBoarThumb.on( 'click', function(e) {
      var $elemParent = $(this).parent(),
          $elemDataCost = $(this).data( 'set-cost' ),
          $elemDataSet = $(this).data( 'set' );

      $(this).addClass( className ).siblings().removeClass( className );
      $(colorPickerProductClass).eq( $(this).index() ).addClass( className ).siblings().removeClass( className );

      setDataCurrentPrice( $elemDataCost, $elemDataSet );

    } );

    $productBoarThumb.on( 'keypress', function(e) {

      if( e.keyCode == 13 ){
        e.preventDefault();
        $(this).toggleClass( className ).siblings().removeClass( className );
        setDataCurrentPrice( $(this).data('set-cost'), $(this).data('set') );
      }
    } )

    // Set data current price by data sec cost
    function setDataCurrentPrice( price, set ) {
      $('[' + currentPriceAttr + ']').attr( currentPriceAttr, price );
      $('[' + currentPriceAttr + ']').attr( 'data-current-set', set );
      $('.js-productModuleBoard input.total').first().val( 1 );
      $('[data-quantity]').attr('data-quantity', 1);
      $('[' + totalCostAttr + ']').attr( totalCostAttr, price );
    }
  }

  var pageId = document.querySelector('[data-page-id]') ? document.querySelector('[data-page-id]').dataset.pageId : null,
      cookieName = 'magniclick_cart_options',
      cartObj = null;

  if( pageId ) {
    cartObj = {
      [pageId]: {}
    }
  }

  if( $cartBtn ) {

    var errorLogContainer = document.querySelector( '.js-erroLog' ),
        errorLog = {
          addClass: function() {
            errorLogContainer.classList.add('error');
          },
          removeClass: function() {
            errorLogContainer.classList.remove('error');
          },
          emptyCost: function() {
            if( Number(document.querySelector('[data-total-cost]').dataset.totalCost ) == 0 ) {
              errorLogContainer.innerText = 'Пожалуйста, выберите минимум 1 единицу товара.';
              this.addClass();
              return true;
            }
          },
          matchSize: function() {
            var inputs = document.querySelectorAll('.js-colorPickerProduct.active input.total').length ? document.querySelectorAll('.js-colorPickerProduct.active input.total') : null,
                maxSet = document.querySelector('[data-current-set]') ? Number(document.querySelector('[data-current-set]').dataset.currentSet) : null,
                totalSum = 0;


            if( inputs ) {
              for( var i = 0; i < inputs.length; i++ ) {
                totalSum += Number(inputs[i].value);
              }

              if( totalSum > maxSet ) {
                errorLogContainer.innerText = 'Превышено допустимое число единиц товара в наборе. Выбрано ' + totalSum + ', допустимое значение ' + maxSet;
                this.addClass();
                return true;
              }
            }

          },
          minSize: function() {
            var inputs = document.querySelectorAll('.js-colorPickerProduct.active input.total').length ? document.querySelectorAll('.js-colorPickerProduct.active input.total') : null,
                maxSet = document.querySelector('[data-current-set]') ? Number(document.querySelector('[data-current-set]').dataset.currentSet) : null,
                totalSum = 0;


            if( inputs ) {
              for( var i = 0; i < inputs.length; i++ ) {
                totalSum += Number(inputs[i].value);
              }

              if( totalSum < maxSet ) {
                errorLogContainer.innerText = 'Не выбранно нужное кол-во единиц товара в наборе. Допустимое кол-во ' + maxSet + '. Выбрано ' + totalSum + '. Осталось ' + ( maxSet - totalSum ) + '.';
                this.addClass();
                return true;
              }
            }

          },
        };

    // Onsubmit cart button
    $cartBtn.on( 'click', function(e) {
      if(errorLog.emptyCost() || errorLog.matchSize() || errorLog.minSize()) removeErrorClass();
      if(errorLog.emptyCost()) return false;
      if(errorLog.matchSize()) return false;
      if(errorLog.minSize()) return false;

      e.preventDefault();

      function Order() {
        this.pageName = document.querySelector('[data-page-name]') ? document.querySelector('[data-page-name]').dataset.pageName : '';
        this.pageId = document.querySelector('[data-page-id]') ? Number(document.querySelector('[data-page-id]').dataset.pageId) : null;
        this.orderCovers = function() {
          if( $('[data-name]').length ) {
            var $elem = $('[data-name]'),
                $cover = [];

            $elem.each( function() {
              var $total = $(this).find('.total').val(),
                  $elemData = $(this).data('name');

              if( $total > 0 ) {
                $cover.push( 'x' + $total + '_' + $elemData  );
              }

            } );

            if( $cover.length ) {
              $cover = $cover.join(';');
              return $cover;
            }else {
              return false;
            }
          }else {
            return false;
          }
        };
        this.orderSet = document.querySelector('[data-current-set]') ? 'kit:' + document.querySelector('[data-current-set]').dataset.currentSet : null;
        this.orderColors = function() {
          if( $('.active input.total').length ) {
            var $set = [],
                $colors = $('.active input.total');

            $colors.each(function() {
              var $elemColor = $(this).parent().data('set-color'),
                  $elemTotal = $(this).val();

              if( $elemTotal > 0 && $elemColor ) {
                $set.push( $elemColor + '-' + $elemTotal );
              }
            });

            if( $set.length ) {
              $set = $set.join(';');
              return $set;
            }else {
              return false;
            }
          }else {
            return false;
          }
        };
        this.orderQuantity = document.querySelector('[data-quantity]') ? Number(document.querySelector('[data-quantity]').dataset.quantity) : 1;
        this.orderTotalCost = document.querySelector('[data-total-cost]') ? Number(document.querySelector('[data-total-cost]').dataset.totalCost) : '';
        this.specialCards = function() {
          var sticks = document.querySelectorAll('[data-stick-line]').length ? document.querySelectorAll('[data-stick-line]') : null;
          if( sticks ) {
            var stickLines = [];

            for( var i = 0; i < sticks.length; i++ ) {
              stickLines.push( sticks[i].dataset.stickLine );
            }

            stickLines = stickLines.join('; ');

            return stickLines;
          }else {
            return false;
          }
        }
      };

      var order = new Order(),
          currentOrder = {},
          cartInfo = {};

      for( var key in order ) {
        if( order[key] ) {
          currentOrder[key] = order[key];
          if( typeof order[key] == "function" ) {
            currentOrder[key] = order[key]();
          }
        }
      }

      for( var key in currentOrder ) {
        if( currentOrder[key] ) {
          cartInfo[key] = currentOrder[key];
        }
      }

      setCartCookie( cartInfo );

    } );
  }

  function removeErrorClass() {
    if( errorLogContainer.classList.contains('error') ) {
      $('.product-board').on( 'click', function(e) {
        if( !e.target.classList.contains('js-cartBtn') ) {
          errorLogContainer.classList.remove('error');
          $(this).unbind('click');
        }
      } );
    }
  }

  // Set Cart Cookie
  function setCartCookie( order ) {
    var order = order;
    if( !checkCookie() ) {
      var currentOrder = {
        [pageId]: {
          [keyNaming( order )]: order,
        },
        cartTotal: order.orderQuantity
      }
      setCookie( currentOrder );
    }else {
      var cookieData = JSON.parse( localStorage.getItem( cookieName ) ),
          objectNameKey = keyNaming( order );

      // If the page id exists in the cookie
      if( !$.isEmptyObject( cookieData[pageId] ) ) {

        // If the order does not exist in this entry
        if( $.isEmptyObject(cookieData[pageId][objectNameKey]) ) {
          cookieData[pageId][objectNameKey] = order;
        }else {
          cookieData[pageId][objectNameKey].orderQuantity += order.orderQuantity;
          cookieData[pageId][objectNameKey].orderTotalCost += order.orderTotalCost;
        }
        cookieData.cartTotal += Number(order.orderQuantity);

      }else {
        var updateCartTotal = cookieData.cartTotal + order.orderQuantity;
        cookieData[pageId] = {
          [keyNaming( order )]: order
        };
        cookieData.cartTotal = updateCartTotal;
      }
      setCookie( cookieData );
    }

    buttonSuccess();
    setTimeout(function() {
      updateCart();
    },1000);

    // return false;
  }

  // Check Cookie
  function checkCookie() {
    if( localStorage.getItem( cookieName ) ) {
      return localStorage.getItem( cookieName );
    }else {
      return false;
    }
  }

  // Set cookies
  function setCookie( cookie ) {
    localStorage.setItem( cookieName, JSON.stringify( cookie ) );
  }

  if( localStorage.getItem( cookieName ) ) {
    setServerSession( localStorage.getItem( cookieName ) );
    $('#cart').removeClass( 'empty-cart' ).find( '.cart-total' ).text( JSON.parse(localStorage.getItem( cookieName )).cartTotal );
    $('.js-cartFormCol').removeClass('hidden');
  }else {
    setEmptyCartPage();
  }

  function setServerSession( cookie ) {
    var cookie = cookie;
    $.ajax({
      url: cartAjaxUrl.url,
      type: 'POST',
      data: ({
              action: 'magniclick_set_cart_session',
              cart_session: cookie
            }),
      dataType: 'html',
      success: function( data ) {
        $('#cart-content').html( data );
        setTimeout( function() {
          inputEvents();
        }, 1000 );
      },
      error: function() {
        $('#cart-content').html( "Нет ответа от сервера. Попробуйте повторить попытку чуть позже." );
      }
    })
  }

  // Remove cookies
  function removeCookie() {
    localStorage.removeItem( cookieName );
  }

  // Creates a unique object name based on keys
  function keyNaming( obj ) {
    var arr = [];

    for( var key in obj ) {
      if( key != 'orderQuantity' && key != 'orderTotalCost' ) {
        arr.push( (key + '-' + obj[key]).trim() );
      }
    }

    return arr.join('_');
  }

  // Update cart when product added
  function updateCart() {
    var currentCookie;
    if( currentCookie = checkCookie() ) {
      var cartTotal = JSON.parse(currentCookie).cartTotal ? JSON.parse(currentCookie).cartTotal : null;

      if( cartTotal ) {
        $('#cart').removeClass( 'empty-cart' ).find( '.cart-total' ).text( cartTotal );
      }

    }
  }

  // Button Success Action
  function buttonSuccess() {
    var $currentBtnText = $cartBtn.text();
    $cartBtn.prop('disabled', true)
            .addClass('waiting');

    setTimeout( function() {
      $cartBtn.removeClass('waiting')
              .addClass('success')
              .text('Добавлено');

    }, 1000 );
    setTimeout( function() {
      $cartBtn.removeClass('success')
              .prop('disabled', false)
              .text( $currentBtnText );

    }, 5000 );
  }

  // CART FUNCTIONS

  function checkLastCartElem() {
    if( document.querySelectorAll('.js-cartListItem').length == 0 ) {

      removeCookie();
      setEmpytCart();
      setEmptyCartPage();
    }
  }

  // Set Empty Cart
  function setEmpytCart() {

    $('#cart').addClass('empty-cart').find( '.cart-total' ).text( '' );
  }
  // Set Empty Cart Page
  function setEmptyCartPage() {
    $('.cart-page-wrapper .container').html('<div class="empty-cart"><h1 class="block-title-md">Корзина пуста</h1><div class="empty-cart-img"><img src="' + ( $('[data-empty-cart-img-url]').length ? $('[data-empty-cart-img-url]').data('empty-cart-img-url') : '' ) + '" alt="Корзина пуста"></div></div>');
  }

  // Update Total Cost
  function updateCartTotal( sum, action ) {
    var price = document.querySelector( '[data-total-price]' ) ? document.querySelector( '[data-total-price]' ) : null;

    if( price ) {
      var dataPrice = Number(price.dataset.totalPrice);

      if( action == 'plus' ) {
        price.dataset.totalPrice = Number(price.dataset.totalPrice) + sum;
      }else if( action == 'minus' ) {
        price.dataset.totalPrice = Number(price.dataset.totalPrice) - sum;
      }
    }
  }

  function findAncestor (el, cls) {
    while ((el = el.parentElement) && !el.classList.contains(cls));
    return el;
  }

  // Check Cart Parent
  function checkCartParent( $elem, type ) {

    if( $elem.parents('.cart-list').length ) {
      var parent = findAncestor( $elem[0], 'js-cartListItem' ),
          parentChild = parent.querySelector('[data-temp-cost]'),
          elemCurrentPrice = parent ? Number(parent.querySelector('[data-object-cost]').dataset.objectCost) : '',
          elemTempPrice = parent ? Number(parent.querySelector('[data-temp-cost]').dataset.tempCost) : '';

      if( type == 'minus' && elemTempPrice ) {
        if( elemTempPrice <= elemCurrentPrice  ) {
          return;
        }else {
          parentChild.dataset.tempCost = ( elemTempPrice - elemCurrentPrice  );
        }
      }else if( type == 'blur' ) {
        var $elemVal = $elem.val();

        if( $elemVal == 0 ) {
          $elem.val(1);
          parentChild.dataset.tempCost = elemCurrentPrice;
        }else {
          parentChild.dataset.tempCost = elemCurrentPrice * ( Number( $(parent).find('input.total').val() ) );
        }
      }else {
        parentChild.dataset.tempCost = elemCurrentPrice * ( Number( $(parent).find('input.total').val() ) );
      }

      var totalQuantity = 0,
          totalSum = 0,
          totalQuantityElem = document.querySelectorAll('.js-cartListItem input.total'),
          totalSumELem = document.querySelectorAll('[data-temp-cost]');

      for( var i = 0; i < totalSumELem.length; i++ ) {
        totalSum += Number(totalSumELem[i].dataset.tempCost);
      }

      for( var i = 0; i < totalQuantityElem.length; i++ ) {
        totalQuantity += Number(totalQuantityElem[i].value);
      }

      updateCookieElem( parent, totalQuantity );

      document.querySelector('[data-total-price]').dataset.totalPrice =  totalSum;
    }
  }

  function updateCookieElem( elem, cartTotal ){
    var objId = elem.dataset.objectId,
        objKey = elem.dataset.objectKey,
        objTempCost = Number(elem.querySelector('[data-temp-cost]').dataset.tempCost),
        objVal = Number(elem.querySelector('input.total').value),
        cookie = checkCookie();


    if( cookie ) {
      var cookie = JSON.parse( cookie );

      cookie[objId][objKey].orderQuantity = objVal;
      cookie[objId][objKey].orderTotalCost = objTempCost;
      cookie.cartTotal = cartTotal;

      setCookie( cookie );
      updateCart();
    }

  }

  if( $mainCartBtn ) {

    $mainCartBtn.on( 'click', function() {
      var $elemFormInput = $(this).parents('form').find('[name=magniclick-form-cart-total]'),
          cookie = checkCookie(),
          result,
          tempCart = [];

      if( cookie ) {
        cookie = JSON.parse( cookie );
      }

      for( var key in cookie ) {

        for( var inKey in cookie[key] ) {
          tempCart.push(cookie[key][inKey] );
        }
      }

      tempCart = JSON.stringify(tempCart);

      result = str_replace( 
                            [ '[', ']', ',', '_', '"pageName":', '"pageId":', '"orderCovers":"', '"orderSet":"kit:', '"orderQuantity":', '"orderTotalCost":', '"orderColors":', 'cartTotal', ':{', '{', '}'],
                            [ '', '', ', ', ' ', 'Имя продукта: ', 'ID-страницы: ', 'Покрытие: ',  'Набор: ', 'Кол-во продукта: ', 'Общая сумма: ', 'Цвета и кол-во: ', 'Общее кол-во продуктов', ':\n{', '\n{', ' }'],
                             tempCart );

      $elemFormInput.val( result );
    } );
  }

  // END CART FUNCTIONS


  // Contacts Form Mail Sent
  document.addEventListener( 'wpcf7mailsent', function( event ) {
    var cartPage = document.getElementById('cart-page');

    if( !cartPage ) {
      $.fancybox.open($('#send-modal'), {baseClass: 'modal-layout'});
    }else {
      $.fancybox.open($('#send-cart-modal'), {baseClass: 'cart-modal-layout'});
      removeCookie();
      setEmpytCart();
      setEmptyCartPage();
    }

    $('form').find('[type="submit"]').prop('disabled', true);
    $('form').find('[type="checkbox"]').prop('checked', false);
    setTimeout( function() {
      removeChecked();
    }, 1000 );
  }, false );

  function removeChecked() {
    // Antispam checbox
    var formCheckAntispam = document.querySelectorAll( '.js-agree' );if( formCheckAntispam ){ for( var i = 0; i < formCheckAntispam.length; i++ ){ formCheckAntispam[i].checked=false; }}
  }

  removeChecked();

  // Cover Tabs
  $('.js-productModuleBoard li[data-name]').on( 'click', function(e) {
    e.preventDefault();
    var $elemDesc = $(this).data('name'),
        $elemDataIndex = $(this).data('index'),
        className = 'active-thumb';

    // Main slider thumb trigger
    if( $elemDataIndex !== '' && !( $elemDataIndex <= 0 ) ){
      $('.js-productJumbThumb li').eq( $elemDataIndex - 1 ).trigger('click');
    }

    // Fucus on IMG Tag
    if( e.target.tagName == 'IMG') {
      $(this).addClass( className ).siblings().removeClass( className );
      $('.control-desc').text( $elemDesc ).attr('data-set-desc', $elemDesc);
    }

  } );


  // Open Contacts
  if( $openContacts ) {
    var openContactsClass = 'open-contact-popup';

    $openContacts.on( 'click', function(e) {
      e.preventDefault();

      // If Contacts Form
      if( $contacts ) {
        $body.addClass( openContactsClass );
      }
    } );

    // If Close Form
    if( $closeContacts ) {

      $closeContacts.on( 'click', function() {
        $body.removeClass( openContactsClass );
      } );
    }
  }

  if( $closeCartPopup ) {

    $closeCartPopup.on( 'click', function( e ) {
      e.preventDefault();
      $(this).parents( '.cart-popup' ).hide();
    } );
  }

  // FAQ Line
  if( $faqLine ) {

    $faqLine.on( 'click', function(e) {
      e.preventDefault();
      $(this).toggleClass('open').parent().find('.js-faqLineAnswer').toggleClass('hidden');
    } );

  }

  // CUSTOM STR REPLACE FUNCTION
  function str_replace (search, replace, subject, countObj) {
    var i = 0,j = 0,temp = '',repl = '',sl = 0,fl = 0,f = [].concat(search),r = [].concat(replace),s = subject,ra = Object.prototype.toString.call(r) === '[object Array]',sa = Object.prototype.toString.call(s) === '[object Array]';
    s = [].concat(s);

    var $global = (typeof window !== 'undefined' ? window : global);
    $global.$locutus = $global.$locutus || {};
    var $locutus = $global.$locutus;
    $locutus.php = $locutus.php || {};

    if (typeof (search) === 'object' && typeof (replace) === 'string') {
      temp = replace;
      replace = [];
      for (i = 0; i < search.length; i += 1) {
        replace[i] = temp;
      }
      temp = '';
      r = [].concat(replace);
      ra = Object.prototype.toString.call(r) === '[object Array]';
    }

    if (typeof countObj !== 'undefined') {
      countObj.value = 0;
    }

    for (i = 0, sl = s.length; i < sl; i++) {
      if (s[i] === '') {
        continue;
      }
      for (j = 0, fl = f.length; j < fl; j++) {
        temp = s[i] + '';
        repl = ra ? (r[j] !== undefined ? r[j] : '') : r[0];
        s[i] = (temp).split(f[j]).join(repl);
        if (typeof countObj !== 'undefined') {
          countObj.value += ((temp.split(f[j])).length - 1);
        }
      }
    }
    return sa ? s : s[0];
  }

} );
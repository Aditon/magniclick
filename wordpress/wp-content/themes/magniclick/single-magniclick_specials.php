<?php

  get_header();
?>

  <?php get_template_part( 'template-parts/content', 'jumb' ) ?>

  <section class="sec">
    <div class="container">
      <?php if( get_field('magniclick_speacil_title') ) {?>
        <h2 class="block-title block-title-md"><?php echo wp_kses_post( get_field('magniclick_speacil_title') ); ?></h2>
      <?php } ?>

      <?php if( get_field('magniclick_special_desc') ) {?>
        <p><?php echo wp_kses_post( get_field('magniclick_special_desc') ); ?></p>
      <?php } ?>
    </div>
  </section>

  <?php get_template_part( 'template-parts/content', 'more-specials' ) ?>

<?php
  get_footer();
?>
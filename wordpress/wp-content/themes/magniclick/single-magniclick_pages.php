<?php

  get_header();
?>

  <?php if( have_posts() ) : while( have_posts() ) : the_post(); ?>
    <div class="jumb page-jumb main-font" style="background-image: url( '<?php echo esc_url( get_the_post_thumbnail_url() ); ?>' )">
      <div class="container">
        <?php if( get_the_content() ) {?>
          <div class="page-jumb__desc">
            <?php echo wp_kses_post( get_the_content() ); ?>
          </div>
        <?php } ?>

        <h1><?php echo wp_kses_post( get_the_title() ); ?></h1>

      </div>
    </div>
  <?php endwhile; endif; ?>

  <?php get_template_part( 'template-parts/content', 'desc-line' ) ?>

  <?php get_template_part( 'template-parts/content', 'more-products' ) ?>

<?php
  get_footer();
?>
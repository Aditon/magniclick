<?php

  /*
    Template Name: Homepage
  */

  get_header();
?>

<!-- Jumbotron -->
<div class="jumb jumb--main-page main-font" style="background-image: url( <?php echo esc_url( get_the_post_thumbnail_url( get_the_ID(), 'main-page-jumb' ) ); ?> );">
  <div class="container">
    <div class="jumb--main-page__content-wrapper">
      <div class="jumb--main-page__desc">
        <?php if( have_posts() ) : while( have_posts() ) : the_post(); ?>

          <h1><?php echo wp_kses_post( get_the_title() ); ?></h1>

          <?php echo wp_kses_post( get_the_content() ); ?>
        <?php endwhile; endif; ?>
      </div>

      <div class="jumb--main-page__bottom-thumb">
        <div class="row">

          <?php if( get_field('magniclick_homepage_video_link') ) {?>
            <div class="col-2">
              <a class="main-page__play-link" data-fancybox href="<?php echo esc_url( get_field('magniclick_homepage_video_link') ); ?>">
                <div class="icon yt-play-icon">
                  <?php if( get_field('magniclick_home_video_icon') ) {?>
                    <img src="<?php echo esc_url( get_field('magniclick_home_video_icon') ) ?>" alt="">
                  <?php } ?>

                  <?php if( get_field('magniclick_home_video_icon_hover') ) {?>
                    <img src="<?php echo esc_url( get_field('magniclick_home_video_icon_hover') ) ?>" alt="">
                  <?php } ?>

                </div>
              </a>
            </div>
          <?php } ?>

          <?php
            $args = array(
              'post_type' => 'magniclick_pages',
              'posts_per_page' => -1,
              'order' => 'asc'
            );

            $magniclick_pages = new WP_Query( $args );
          ?>

          <?php if( $magniclick_pages->have_posts() ) : ?>
            <div class="col-10">
              <ul class="main-page__thumb-list">
                <?php while( $magniclick_pages->have_posts() ) : $magniclick_pages->the_post(); ?>
                  <li>
                    <a href="<?php echo esc_url( get_the_permalink() ); ?>">
                      <div class="icon team-icon-white">
                        <?php if( get_field('magniclick_main_page_svg') ) {?>
                          <img src="<?php echo esc_url( get_field('magniclick_main_page_svg') ); ?>" alt="<?php echo wp_kses_post( get_the_title() ); ?>">
                        <?php } ?>

                        <?php if( get_field('magniclick_main_page_svg_hover') ) {?>
                          <img src="<?php echo esc_url( get_field('magniclick_main_page_svg_hover') ); ?>" alt="<?php echo wp_kses_post( get_the_title() ); ?>">
                        <?php } ?>
                      </div>
                      <span><?php echo wp_kses_post( get_the_title() ); ?></span>
                    </a>
                  </li>
                <?php endwhile; ?>
              </ul>
            </div>
          <?php endif; ?>
        </div>
      </div>

    </div>
  </div>
</div>


<!-- Products -->
<section class="products-block sec">
  <div class="container">
    <h2 class="block-title block-title-md text-center"><?php esc_html_e( 'Наши продукты', 'magniclick' ); ?></h2>

    <?php
      $args = array(
        'post_type' => 'magniclick_products',
        'posts_per_page' => -1,
        'order' => 'asc'
      );

      $products = new WP_Query( $args );
    ?>

    <?php if( $products->have_posts() ) : $i = 1; ?>
      <div class="products main-font">
        <div class="ms-grid js-msGrid">

          <?php $img_size = ''; while( $products->have_posts() ) : $products->the_post();
            if( $i == 1 ) {
              $img_size = 'main-page-product-1' ;
            }else {
              $img_size = 'main-page-product-2' ;
            }
          ?>

            <a href="<?php echo esc_url( get_the_permalink() ); ?>" class="item grid-item">
              <div class="product product--long">
                <div class="product__desc">
                  <span class="product__text"><?php echo wp_kses_post( get_the_title() ); ?></span>
                  <span class="btn more-btn">Подробнее</span>
                </div>

                <div class="img-block">
                  <?php if( $i == 1 ) {?>
                    <picture>
                      <source srcset="<?php echo esc_url( get_the_post_thumbnail_url( get_the_ID(), 'main-page-product-2' ) ); ?>" media="(max-width: 480px)">
                      <img src="<?php echo esc_url( get_the_post_thumbnail_url( get_the_ID(), $img_size ) ); ?>" alt="<?php echo wp_kses_post( get_the_title() ); ?>">
                    </picture>
                  <?php }else {?>
                    <img src="<?php echo esc_url( get_the_post_thumbnail_url( get_the_ID(), $img_size ) ); ?>" alt="<?php echo wp_kses_post( get_the_title() ); ?>">
                  <?php } ?>
                </div>

              </div>
            </a>

          <?php $i++; endwhile; ?>

        </div>
      </div>
    <?php endif; wp_reset_query(); ?>

  </div>
</section>

<?php get_template_part('template-parts/content', 'specials'); ?>

<?php get_template_part('template-parts/content', 'instagram'); ?>

<?php get_footer(); ?>
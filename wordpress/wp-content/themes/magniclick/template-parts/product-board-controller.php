<?php $value = 0; if( isset( $controller_total ) and !empty( $controller_total ) and is_numeric( $controller_total ) ) { $value = $controller_total; } ?>

<div class="number-control js-numberControl" data-total="<?php echo esc_attr( $value ); ?>" data-quantity="<?php echo esc_attr( $value ); ?>">
  <button class="btn minus">&#8722;</button>
  <input class="total" type="text" value="<?php echo esc_attr( $value ); ?>">
  <button class="btn plus">+</button>
</div>
<?php $page_id = get_the_ID(); ?>
<section class="sec">
  <div class="container">
    <h2 class="block-title block-title-md"><?php esc_html_e( 'Другие специальные предложения', 'magniclick' ); ?></h2>

    <?php

      $args = array(
        'post_type' => 'magniclick_specials',
        'posts_per_page' => -1,
        'order' => 'asc'
      );

      $specials = new WP_Query( $args );
    ?>


    <?php if( $specials->have_posts() ) : ?>
      <div class="more-special">
        <div class="row">
          <?php while( $specials->have_posts() ) : $specials->the_post(); if( $page_id !== get_the_ID() ) {
            $total = 0;
            $currency = get_field( 'magniclick_product_currency' ) ? wp_kses_post( get_field( 'magniclick_product_currency' ) ) : '';
            $cost = get_field('magniclick_product_cost') ? (int)get_field('magniclick_product_cost') : null;
            $discount = get_field('magniclick_product_discount') ? (int)get_field('magniclick_product_discount') : null;

            if( $cost and $discount ) {
              $total = $cost - $discount;
            }else {
              $total = $cost;
            }
          ?>


          <div class="col-3">
            <div class="special-card">
              <a class="special-card__inner" href="<?php echo esc_url( get_the_permalink() ); ?>" style="background-color: <?php echo esc_attr( get_field('magniclick_speacial_board_color') ); ?>;">

                <?php if( $discount ) {?>
                  <div class="special-card__discount"><span>Выгода</span><span class="text"><?php echo esc_html( $discount ); ?> <span class="rub"><?php echo $currency; ?></span></span></div>
                <?php } ?>

                <div class="special-card__img img-block">
                  <img src="<?php echo esc_url( get_the_post_thumbnail_url( get_the_ID(), 'special-thumb' ) ); ?>" alt="">
                </div>

                <h6 class="block-title special-card__title"><?php echo wp_kses_post( get_the_title() ); ?></h6>
                <p class="desc">9&nbsp;модулей, 10&nbsp;планшетов, 48&nbsp;стикеров</p>

                <div class="special-card__cost-wrapper">
                  <?php if( $total ) {?>
                    <span class="main-cost"><?php echo esc_html( $total ); ?> <span class="rub"><?php echo $currency; ?></span></span>
                  <?php } ?>

                  <?php if( $cost ) {?>
                    <div class="old-cost"><?php echo esc_html( $cost ); ?> <span class="rub"><?php echo $currency; ?></span></div>
                  <?php } ?>
                </div>
              </a>
            </div>
          </div>
        <?php } endwhile; ?>

        </div>
      </div>
    <?php endif; wp_reset_query(); ?>


  </div>
</section>
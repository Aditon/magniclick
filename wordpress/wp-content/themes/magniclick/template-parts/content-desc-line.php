<div class="container">
  <div class="desc-line">
    <div class="row">
      <div class="col-7 align-self-center">
        <?php if( get_field( 'magniclick_first_untitle' ) ) {?>
          <h2 class="block-title block-title-md desc-line__title"><?php echo wp_kses_post( get_field( 'magniclick_first_untitle' ) ); ?></h2>
        <?php } ?>

        <?php if( get_field( 'magniclick_first_untitle_text' ) ) {?>
          <p><?php echo wp_kses_post( get_field( 'magniclick_first_untitle_text' ) ); ?></p>
        <?php } ?>
      </div>

      <?php if( get_field( 'magniclick_first_desc_img' ) ) {?>
        <div class="col-5">
          <div class="img-block"><?php echo wp_get_attachment_image( get_field( 'magniclick_first_desc_img' ), 'product-desc-line'  ); ?>"</div>
        </div>
      <?php } ?>

      <?php if( get_field( 'magniclick_second_desc_img' ) ) {?>
        <div class="col-5">
          <div class="img-block"><?php echo wp_get_attachment_image( get_field( 'magniclick_second_desc_img' ), 'product-desc-line' ); ?>"</div>
        </div>
      <?php } ?>

      <div class="col-7 align-self-center">
        <?php if( get_field( 'magniclick_second_untitle' ) ) {?>
          <h2 class="block-title block-title-md desc-line__title"><?php echo wp_kses_post( get_field( 'magniclick_second_untitle' ) ); ?></h2>
        <?php } ?>

        <?php if( get_field( 'magniclick_second_untitle_text' ) ) {?>
          <p><?php echo wp_kses_post( get_field( 'magniclick_second_untitle_text' ) ); ?></p>
        <?php } ?>
      </div>

    </div>
  </div>
</div>
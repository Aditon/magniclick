<?php
/**
 * Template part for displaying a message that posts cannot be found
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Maniclick
 */

?>

<section class="no-results not-found">
	<header class="page-header">
		<h1 class="block-title block-title-md page-title"><?php esc_html_e( 'Ничего не найдено', 'magniclick_' ); ?></h1>
	</header><!-- .page-header -->

	<div class="page-content">
		<?php
		if ( is_home() && current_user_can( 'publish_posts' ) ) :

			printf(
				'<p>' . wp_kses(
					/* translators: 1: link to WP admin new post page. */
					__( 'Готовы опубликовать свой первый пост? <a href="%1$s">Начните здесь</a>.', 'magniclick_' ),
					array(
						'a' => array(
							'href' => array(),
						),
					)
				) . '</p>',
				esc_url( admin_url( 'post-new.php' ) )
			);

		elseif ( is_search() ) :
			?>

			<p><?php esc_html_e( 'Извините, но ничто не соответствует вашим поисковым запросам. Пожалуйста, попробуйте еще раз с некоторыми другими ключевыми словами.', 'magniclick_' ); ?></p>
			<?php

		else :
			?>

			<p><?php esc_html_e( 'Похоже, мы не можем найти то, что вы ищете. Возможно, повторный поиск сможет помочь.', 'magniclick_' ); ?></p>
			<?php

		endif;
		?>
	</div><!-- .page-content -->
</section><!-- .no-results -->

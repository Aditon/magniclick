


<?php if( have_rows('magniclick_advant') ): ?>
  <section>
    <div class="container">
      <h2 class="block-title block-title-md"><?php esc_html_e( 'Преимущества', 'magniclick' ) ?></h2>
      <div class="product-advantages main-font">
        <div class="row">

          <?php while( have_rows('magniclick_advant') ): the_row();
            $img = get_sub_field('magniclick_advant_img') ? get_sub_field('magniclick_advant_img') : null;
            $title = get_sub_field('magniclick_advant_title') ? get_sub_field('magniclick_advant_title') : null;
            $desc = get_sub_field('magniclick_advant_desc') ? get_sub_field('magniclick_advant_desc') : null;
          ?>
            <div class="col-3">
              <?php if( $img ) {?>
                <div class="img-block"><img src="<?php echo esc_url($img); ?>" alt=""></div>
              <?php } ?>

              <?php if( $title ) {?>
                <h6 class="title"><?php echo wp_kses_post( $title ); ?></h6>
              <?php } ?>

              <?php if( $desc ) {?>
                <p><?php echo wp_kses_post( $desc ); ?></p>
              <?php } ?>
            </div>

          <?php endwhile; ?>

        </div>
      </div>
    </div>
  </section>
<?php endif; ?>
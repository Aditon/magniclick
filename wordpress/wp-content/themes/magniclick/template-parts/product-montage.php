<?php if( have_rows('magniclick_montage_complect') or have_rows('magniclick_steps')  ): $theme_domain = 'magniclick'; ?>

  <section class="montage-wrapper sec">
    <div class="container">
      <h2 class="block-title block-title-md"><?php esc_html_e( 'Монтаж', $theme_domain ); ?></h2>

      <div class="montage main-font">
        <div class="row">
          <div class="col-6">

            <?php if( have_rows('magniclick_montage_complect') ) : ?>
              <div class="montage__board">
                <h3 class="montage__board-title"><?php esc_html_e( 'В комплекте:', $theme_domain ) ?></h3>
                <div class="row">

                  <?php while( have_rows('magniclick_montage_complect') ): the_row();
                    $img = get_sub_field('magniclick_montage_img') ? get_sub_field('magniclick_montage_img') : null;
                    $desc = get_sub_field('magniclick_montage_desc') ? get_sub_field('magniclick_montage_desc') : null;
                  ?>
                    <div class="col-3">
                      <?php if( $img ) {?>
                        <div class="img-block"><img src="<?php echo esc_url( $img ); ?>" alt="<?php if( $desc ) {echo esc_html($desc); }; ?>"></div>
                      <?php } ?>

                      <?php if( $desc ) {?>
                        <span class="montage__board-desc"><?php echo esc_html($desc); ?></span>
                      <?php } ?>
                    </div>

                  <?php endwhile; ?>
                </div>
              </div>
            <?php endif; ?>

            <?php if( have_rows('magniclick_steps') ) : ?>
              <div class="montage__steps">
                <div class="row">
                  <?php $i = 0; while( have_rows('magniclick_steps') ) : the_row();
                    $img = get_sub_field('magniclick_step_img') ? get_sub_field('magniclick_step_img') : null;
                    $desc = get_sub_field('magniclick_step_desc') ? get_sub_field('magniclick_step_desc') : null;
                  ?>
                    <div class="col-4">
                      <span class="step">Шаг <?php echo esc_html( $i ) ?>.</span>
                      <?php if( $img ) {?>
                        <div class="img-block"><img src="<?php echo esc_url($img); ?>" alt="<?php if( $desc ){echo esc_html($desc);}; ?>"></div>
                      <?php } ?>

                      <?php if( $desc ) {?>
                        <span class="text"><?php echo esc_html($desc); ?></span>
                      <?php } ?>
                    </div>

                  <?php $i++; endwhile; ?>
                </div>
              </div>
            <?php endif; ?>
          </div>

          <?php if( get_field('magniclick_step_video_prev') ) {?>
            <div class="col-6">
              <div class="montage__video-block">
                <?php if( get_field('magniclick_step_video_link') ) { ?>
                  <a data-fancybox href="<?php echo esc_url(get_field('magniclick_step_video_link')); ?>">
                    <div class="img-block">
                      <img src="<?php echo esc_url( get_field('magniclick_step_video_prev')['url']); ?>" alt="Video Prev">
                    </div>
                    <span class="montage__video-play js-montageVideoPlay"><i class="icon video-play-icon"></i></span>
                  </a>
                <?php }else{ ?>
                  <div class="img-block">
                    <img src="<?php echo esc_url( get_field('magniclick_step_video_prev')['url']); ?>" alt="Video Prev">
                  </div>
                  <button class="montage__video-play js-montageVideoPlay"><i class="icon video-play-icon"></i></button>
                <?php } ?>
              </div>
            </div>
          <?php } ?>

        </div>
      </div>
    </div>
  </section>

<?php endif; ?>
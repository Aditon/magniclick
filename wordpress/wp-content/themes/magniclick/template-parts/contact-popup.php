<?php global $magniclick; ?>
<div class="contact-form-popup js-contactsPopup main-font">

  <button class="contact-form__close-popup js-closePopup"><i class="icon close-icon-white"></i></button>

  <div class="contact-form">
    <div class="row">

      <div class="col-6">
        <h5 class="contact-form__title">Наши контакты</h5>
        
        <?php if( $magniclick['phone-number'] ) {?>
          <div class="site-phone contact-form__item">
            <a href="tel:<?php echo esc_html( str_replace( [ ' ', '-', '(', ')' ], '', $magniclick['phone-number']) ); ?>">
              <i class="icon phone-icon-white" aria-hidden="true"></i>
              <?php echo esc_html($magniclick['phone-number']); ?>
            </a>
          </div>
        <?php } ?>
        
        <?php if( $magniclick['mail-address'] ) {?>
          <div class="site-email contact-form__item">
            <a href="mailto:<?php echo esc_html($magniclick['mail-address']); ?>">
              <i class="icon mail-icon-white" aria-hidden="true"></i>
              <?php echo esc_html($magniclick['mail-address']); ?>
            </a>
          </div>
        <?php } ?>

        <?php if( $magniclick['viber-link'] or $magniclick['telegram-link'] or $magniclick['whatsapp-link'] ) {?>
          <div class="social-block contact-form__item">

            <?php if( $magniclick['viber-link'] ) {?>
              <a class="social-link" href="<?php echo esc_url($magniclick['viber-link']); ?>">
                <i class="icon viber-icon-white" aria-hidden="true"></i>
              </a>
            <?php } ?>

            <?php if( $magniclick['telegram-link'] ) {?>
              <a class="social-link" href="<?php echo esc_url($magniclick['telegram-link']); ?>">
                <i class="icon telegram-icon-white" aria-hidden="true"></i>
              </a>
            <?php } ?>

            <?php if( $magniclick['whatsapp-link'] ) {?>
              <a class="social-link" href="<?php echo esc_url($magniclick['whatsapp-link']); ?>">
                <i class="icon whatsapp-icon-white" aria-hidden="true"></i>
              </a>
            <?php } ?>

            <?php if( $magniclick['phone-number'] ) {?>
              <a href="tel:<?php echo esc_html( str_replace( [ ' ', '-', '(', ')' ], '', $magniclick['phone-number']) ); ?>">
                <?php echo esc_html($magniclick['phone-number']); ?>
              </a>
            <?php } ?>
          </div>
        <?php } ?>

        <?php if( $magniclick['time-desc'] ) {?>
          <div class="site-time contact-form__item">
            <i class="icon clock-icon-white" aria-hidden="true"></i>
            <span><?php echo esc_html($magniclick['time-desc']); ?></span>
          </div>
        <?php } ?>
      </div>

      <div class="col-6">
        <h5 class="contact-form__title">Оставьте контакты для связи</h5>
        <div class="white-form">
          <?php echo do_shortcode( '[contact-form-7 id="333" title="Contact Popup"]' ) ?>
        </div>
      </div>
    </div>
  </div>
</div>
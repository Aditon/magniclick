<?php $page_id = get_the_ID();?>
<section class="sec">
  <div class="container">
    <?php if( get_field( 'magniclick_for_what' ) ) {?>
      <h2 class="block-title block-title-md"><?php echo esc_attr( get_field( 'magniclick_for_what' ) ); ?></h2>
    <?php }else{ ?>
      <h2 class="block-title block-title-md"><?php esc_attr_e( 'С этим покупают', 'magniclick' ); ?></h2>
    <?php } ?>

    <?php
      $elems = 5;

      if( !is_singular( 'magniclick_products' ) ) {
        $elems = 4;
      }

      $args = array(
        'post_type' => 'magniclick_products',
        'posts_per_page' => $elems
      );

      $products = new WP_Query( $args );
    ?>

    <?php if( $products->have_posts() ) : ?>
      <div class="more-products main-font">
        <div class="row">

          <?php while( $products->have_posts() ) : $products->the_post();?>

            <?php if( $page_id !== get_the_ID() ) {?>
              <div class="col-3">
                <a href="<?php echo esc_url( get_the_permalink() ); ?>">
                  <div class="img-block"><img src="<?php echo esc_html( get_the_post_thumbnail_url( get_the_ID(), 'more-products' ) ); ?>" alt="<?php echo wp_kses_post( get_the_title() ); ?>"></div>
                </a>
                <a href="<?php echo esc_url( get_the_permalink() ); ?>">
                  <h4 class="more-products__title"><?php echo wp_kses_post( get_the_title() ); ?></h4>
                </a>

                <p> <?php echo esc_attr( get_field('magniclick_product_cost', get_the_ID()) ); ?> <span class="rub"><?php echo wp_kses_post( get_field('magniclick_product_currency', get_the_ID()) ); ?></span></p>
              </div>
            <?php } ?>

          <?php endwhile; ?>

        </div>
      </div>
    <?php endif; wp_reset_query(); ?>

  </div>
</section>
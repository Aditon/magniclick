<div class="jumb product-jumb">
  <?php $gallery = false; if( get_field( 'magniclick_gallery' ) ) { $gallery = get_field( 'magniclick_gallery' );} ?>

  <?php if( $gallery ) {?>

    <div class="product-jumb__slider-arrows js-jumbArrows">

      <button class="prev">
        <svg width="16" height="11" viewBox="0 0 16 11" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path d="M0.0799999 4.62409C0.127594 4.50134 0.19896 4.3892 0.290001 4.29409L4.29 0.294092C4.38324 0.200853 4.49393 0.126893 4.61575 0.0764322C4.73757 0.0259719 4.86814 0 5 0C5.2663 0 5.5217 0.105788 5.71 0.294092C5.8983 0.482395 6.00409 0.73779 6.00409 1.00409C6.00409 1.27039 5.8983 1.52579 5.71 1.71409L3.41 4.00409H15C15.2652 4.00409 15.5196 4.10945 15.7071 4.29699C15.8946 4.48452 16 4.73888 16 5.00409C16 5.26931 15.8946 5.52366 15.7071 5.7112C15.5196 5.89873 15.2652 6.00409 15 6.00409H3.41L5.71 8.29409C5.80373 8.38706 5.87812 8.49765 5.92889 8.61951C5.97966 8.74137 6.0058 8.87208 6.0058 9.00409C6.0058 9.1361 5.97966 9.26681 5.92889 9.38867C5.87812 9.51053 5.80373 9.62113 5.71 9.71409C5.61704 9.80782 5.50644 9.88221 5.38458 9.93298C5.26272 9.98375 5.13201 10.0099 5 10.0099C4.86799 10.0099 4.73728 9.98375 4.61542 9.93298C4.49356 9.88221 4.38296 9.80782 4.29 9.71409L0.290001 5.71409C0.19896 5.61899 0.127594 5.50684 0.0799999 5.38409C-0.0200176 5.14063 -0.0200176 4.86755 0.0799999 4.62409Z" fill="#B7B6BE"/>
        </svg>
      </button>

      <button class="next">
        <svg width="16" height="11" viewBox="0 0 16 11" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path d="M15.92 4.62409C15.8724 4.50134 15.801 4.3892 15.71 4.29409L11.71 0.294092C11.6168 0.200853 11.5061 0.126893 11.3842 0.0764322C11.2624 0.0259719 11.1319 0 11 0C10.7337 0 10.4783 0.105788 10.29 0.294092C10.1017 0.482395 9.99591 0.73779 9.99591 1.00409C9.99591 1.27039 10.1017 1.52579 10.29 1.71409L12.59 4.00409H1C0.734784 4.00409 0.48043 4.10945 0.292893 4.29699C0.105357 4.48452 0 4.73888 0 5.00409C0 5.26931 0.105357 5.52366 0.292893 5.7112C0.48043 5.89873 0.734784 6.00409 1 6.00409H12.59L10.29 8.29409C10.1963 8.38706 10.1219 8.49766 10.0711 8.61951C10.0203 8.74137 9.9942 8.87208 9.9942 9.00409C9.9942 9.1361 10.0203 9.26681 10.0711 9.38867C10.1219 9.51053 10.1963 9.62113 10.29 9.71409C10.383 9.80782 10.4936 9.88221 10.6154 9.93298C10.7373 9.98375 10.868 10.0099 11 10.0099C11.132 10.0099 11.2627 9.98375 11.3846 9.93298C11.5064 9.88221 11.617 9.80782 11.71 9.71409L15.71 5.71409C15.801 5.61899 15.8724 5.50684 15.92 5.38409C16.02 5.14063 16.02 4.86755 15.92 4.62409Z" fill="#B7B6BE"/>
        </svg>
      </button>
    </div>
  <?php } ?>

  <?php if( $gallery ) {?>
    <div class="product-jumb-wrapper">
      <div class="product-jumb__slider owl-carousel js-productJumbSlider" data-slider-id="1">
        <?php foreach( $gallery as $img_link ) { ?>
          <div class="product-jumb__slide item" style="background-image: url(<?php echo esc_url( wp_get_attachment_image_url( $img_link, 'main-page-jumb' ) ); ?>)"></div>
        <?php } ?>
      </div>
    </div>
  <?php } ?>

  <div class="product-jumb__main-info">
    <div class="main-container">
      <div class="container">
        <div class="row">
          <div class="col-6 product-board-col">

            <?php get_template_part( 'template-parts/product', 'board' ) ?>

          </div>

          <?php if( $gallery ) {
              $cols = 4;

              if( count( $gallery ) > 4 ) {
                $cols = count( $gallery );
              }
            ?>
            <div class="col-6 product-jumb-thumb-col">
              <ul class="product-jumb-thumb js-productJumbThumb" data-slider-id="1">
                <?php foreach( $gallery as $img_thumb ) {?>
                  <li class="item col-<?php echo $cols; ?> "><?php echo wp_get_attachment_image( $img_thumb, 'product-slide-thumb' ); ?></li>
                <?php } ?>
              </ul>
            </div>
          <?php } ?>

        </div>
      </div>
    </div>
  </div>
</div>

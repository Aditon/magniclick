<?php global $products;  if( $products->have_posts() ) : $i = 1; ?>
  <div class="products main-font">
    <div class="ms-grid js-msGrid">

      <?php $img_size = ''; while( $products->have_posts() ) : $products->the_post();
        if( $i == 1 ) {
          $img_size = 'main-page-product-1' ;
        }else {
          $img_size = 'main-page-product-2' ;
        }
      ?>

        <a href="<?php echo esc_url( get_the_permalink() ); ?>" class="item grid-item">
          <div class="product product--long">
            <div class="product__desc">
              <span class="product__text"><?php echo wp_kses_post( get_the_title() ); ?></span>
              <span class="btn more-btn">Подробнее</span>
            </div>

            <div class="img-block">
              <?php if( $i == 1 ) {?>
                <picture>
                  <source srcset="<?php echo esc_url( get_the_post_thumbnail_url( get_the_ID(), 'main-page-product-2' ) ); ?>" media="(max-width: 480px)">
                  <img src="<?php echo esc_url( get_the_post_thumbnail_url( get_the_ID(), $img_size ) ); ?>" alt="<?php echo wp_kses_post( get_the_title() ); ?>">
                </picture>
              <?php }else {?>
                <img src="<?php echo esc_url( get_the_post_thumbnail_url( get_the_ID(), $img_size ) ); ?>" alt="<?php echo wp_kses_post( get_the_title() ); ?>">
              <?php } ?>
            </div>

          </div>
        </a>

      <?php $i++; endwhile; ?>

    </div>
  </div>
<?php endif; wp_reset_query(); ?>
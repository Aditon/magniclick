<?php global $magniclick; $main_total_cost = 0; $active_class = ' active'; $controller = true; ?>

<div class="product-board main-font" data-page-id="<?php echo esc_attr(get_the_ID()); ?>" data-page-name="<?php echo esc_attr( str_replace( ' ', '_', get_the_title() ) ); ?>">
  <?php if( have_posts() ) : while( have_posts() ) : the_post(); ?>
    <h1><?php echo esc_html( get_the_title() ); ?></h1>

    <?php
      if( get_field('magniclick_product_cost') ) {
        $main_total_cost = get_field('magniclick_product_cost');
      }
    ?>

    <?php if( get_field( 'magniclick_module_board_desc' ) ) {?>
      <p class="product-board__desc"><?php echo wp_kses_post( get_field('magniclick_module_board_desc') ); ?></p>
    <?php } ?>

    <?php if( get_field( 'magniclick_product_availability' ) ) { ?>

          <span class="product-board__stock"><?php esc_html_e( 'есть в наличии', 'magniclick' ) ?></span>
        <?php }else { ?>
      <span class="product-board__stock"><?php esc_html_e( 'отсутствует', 'magniclick' ) ?></span>
    <?php } ?>

    <?php if( have_rows('magniclick_module_board_type') ) :
      $count = 0;
      $first_cost =[];
      while( have_rows('magniclick_module_board_type') ) : the_row();

        if( get_sub_field('magniclick_module_board_cost') ) {
          $first_cost[] = get_sub_field('magniclick_module_board_cost');
        }
      endwhile;

    ?>
      <!-- MODULE BOARDS -->
      <div class="board-props js-moduleBoard" data-module-quantity="1">
        <div class="product-cover">
          <div class="control-wrapper <?php echo esc_attr($active_class); ?>">
            <?php if( get_field('magniclick_module_board_over_type') ) { $controller = false; $main_total_cost = $first_cost[0] ? $first_cost[0] : $main_total_cost;?>
              <div class="product-cover__desc"><strong><span class="control-name">Покрытие:</span></strong><span class="control-desc" data-set-desc="<?php echo esc_attr( get_field('magniclick_module_board_over_type') ); ?>"><?php echo esc_html( get_field('magniclick_module_board_over_type') ); ?></span></div>
            <?php } ?>

              <div class="input-group">
                <ul class="product-cover__items js-productModuleBoard">
                  <?php while( have_rows('magniclick_module_board_type') ) : the_row(); ?>

                    <li tabindex="0" aria-label="product-cover" data-current-price="<?php echo esc_attr($first_cost[$count]); ?>" data-name="<?php echo esc_attr(get_sub_field('magniclick_module_board_form_desc')); ?>" data-index="<?php echo esc_attr(get_sub_field('magniclick_module_board_thumb_Index')); ?>" <?php if($count == 0 ) {echo $active_class;} ?> >
                      <div class="row align-items-center">
                        <div class="col">
                          <div class="board-thumb-items img-block">
                            <img src="<?php echo esc_url(get_sub_field('magniclick_module_board')); ?>" alt="">
                          </div>
                        </div>

                        <div class="col">
                          <div class="control-wrapper">
                            <div class="number-control js-numberControl" data-total="<?php if($count == 0 ) {echo '1';}else {echo '0';} ?>" data-temp-total="<?php if($count == 0 ) {echo '1';}else {echo '0';} ?>">
                              <button class="btn minus">&#8722;</button>
                              <input class="total" type="text" value="<?php if($count == 0 ) {echo '1';}else {echo '0';} ?>">
                              <button class="btn plus">+</button>
                            </div>
                          </div>
                        </div>

                        <?php if( get_sub_field('magniclick_module_board_cost') ) {?>
                          <div class="col">
                            <div class="product-cost" data-cover-cost="<?php echo esc_attr(get_sub_field('magniclick_module_board_cost')); ?>" >
                              <span class="rub">&#8381;</span>
                            </div>
                          </div>
                        <?php } ?>

                      </div>
                    </li>

                  <?php $count++; endwhile; ?>
                </ul>
              </div>
          </div>
        </div>
      </div>
      <!-- END MODULE BOARDS -->
    <?php endif; ?>

    <?php if( have_rows('magniclick_magnit_and_stickers_sets') ) : $i = 0; $controller = false;

        $first_preset = [];
        $first_set = [];
        while( have_rows('magniclick_magnit_and_stickers_sets') ) : the_row();

        if( $i == 0 ) {
          $first_preset[] = get_sub_field('magniclick_magnit_and_stickers_set_cost');
          $first_set[] = get_sub_field('magniclick_magnit_and_stickers_total');
          $i++;
        }else {
          continue;
        }

        endwhile;

        $main_total_cost = $first_preset[0];
      ?>

      <!-- MARKER MAGNITS AND STICKERS -->
      <div class="board-props" data-current-price="<?php echo esc_attr($first_preset[0]); ?>" data-current-set="<?php echo esc_attr($first_set[0]); ?>">
        <div class="control-wrapper"><strong><span class="control-name">Кол-во:</span></strong>
          <ul class="number-preset js-productBoarThumb">

            <?php $i = 0; while( have_rows('magniclick_magnit_and_stickers_sets') ) : the_row(); ?>
              <li class="board-thumb-items<?php if( $i == 0 ) { echo $active_class; } ?>"
                  tabindex="0"
                  aria-label="number-preset"
                  data-set="<?php echo esc_attr(get_sub_field('magniclick_magnit_and_stickers_total')); ?>"
                  data-set-cost="<?php echo esc_attr(get_sub_field('magniclick_magnit_and_stickers_set_cost')); if( $i == 0 ) { $main_total_cost = (int)get_sub_field('magniclick_magnit_and_stickers_set_cost'); } ?>"
                  >
                <?php echo esc_attr(get_sub_field('magniclick_magnit_and_stickers_total')); ?>
              </li>
            <?php $i++; endwhile; ?>
          </ul>
        </div>
      </div>

      <div class="control-wrapper js-productModuleBoard">
        <?php $controller_total = 1; ?>
        <?php include(locate_template( 'template-parts/product-board-controller.php' )); ?>
      </div>

      <!-- END MARKER MAGNITS AND STICKERS -->
    <?php endif; ?>

    <?php $i = 0; if( have_rows('magniclick_complect') ) : $picker_colors_array = []; $controller = false;
        $first_preset = [];
        while( have_rows('magniclick_complect') ) : the_row();

        if( $i == 0 ) {
          $first_preset[] = get_sub_field('magniclick_set_cost');
          $first_set[] = get_sub_field('magniclick_set');
          $i++;
        }else {
          continue;
        }

        endwhile;

        $main_total_cost = $first_preset[0];
      ?>

      <!-- MAGNETIC STICKERS -->
      <div class="board-prop" data-current-price="<?php echo esc_attr($first_preset[0]); ?>" data-current-set="<?php echo esc_attr($first_set[0]); ?>">
        <div class="control-wrapper"><strong><span class="control-name">Кол-во:</span></strong>
          <ul class="number-preset js-productBoarThumb">
            <?php $color_set_count = 0; while( have_rows('magniclick_complect') ) : the_row(); ?>
              <li class="board-thumb-items<?php if( $color_set_count == 0 ) { echo $active_class; } ?>"
                  tabindex="0"
                  aria-label="number-preset"
                  data-set="<?php echo esc_attr(get_sub_field('magniclick_set')); ?>"
                  data-set-cost="<?php echo esc_attr(get_sub_field('magniclick_set_cost'));?>"
                >
                <?php echo esc_html(get_sub_field('magniclick_set')); ?>
              </li>


              <?php
                if( have_rows('magniclick_set_colors') ) : while( have_rows('magniclick_set_colors') ) : the_row();
                  $picker_colors_array[ get_sub_field('magniclick_set_color') ][] = get_sub_field('magniclick_set_color_total');
                endwhile; endif;
              ?>
            <?php $color_set_count++; endwhile; ?>
          </ul>
        </div>
        <div class="control-color-picker-wrapper">
          <div class="control-color-picker">

            <?php for( $i = 0; $i < $color_set_count; $i++ ) {?>
              <div class="color-picker-box  js-colorPickerProduct<?php if( $i == 0 ) {echo $active_class;} ?>">
                <span class="control-name"><strong>Цвета:</strong></span>

                <?php foreach( $picker_colors_array as $color => $total ) {
                  $current_color_num = isset($total[$i]) ? $total[$i] : '0';

                ?>

                  <div class="control-wrapper">
                    <div class="number-control number-control--color js-numberControl" data-set-color="<?php echo esc_attr($color); ?>" data-total="<?php echo esc_attr($current_color_num); ?>" style="background-color: <?php echo esc_attr($color); ?>;">
                      <button class="btn minus">&#8722;</button>
                      <input class="total" type="text" value="<?php echo esc_attr($current_color_num); ?>">
                      <button class="btn plus">+</button>
                    </div>
                  </div>

                <?php } ?>

              </div>
            <?php } ?>
          </div>
        </div>
      </div>
      <!-- END MAGNETIC STICKERS -->
    <?php endif;  ?>


    <?php if( have_rows('magniclick_marker_tablet_colors') ) : $count = 0; $controller = false;?>
      <!-- MARKER TABLETS -->
      <div class="board-props marker-tablets" data-current-price="<?php echo esc_attr($main_total_cost); ?>">
        <div class="product-cover">

          <div class="input-group js-productModuleBoard <?php echo esc_attr($active_class); ?>">
            <div class="control-wrapper">
              <strong><span class="control-name">Кол-во:</span></strong> <span class="quantity" data-module-quantity="1"></span>
            </div>
            <div class="row flex-wrap justify-content-between">
              <?php while( have_rows('magniclick_marker_tablet_colors') ) : the_row(); ?>
                <div class="col">
                  <div class="control-wrapper">
                    <div class="number-control number-control--color js-numberControl" style="background-color: <?php echo esc_attr(get_sub_field('magniclick_set_color')); ?>" data-set-color="<?php echo esc_attr(get_sub_field('magniclick_set_color')); ?>">
                      <button class="btn minus">&#8722;</button>
                      <input class="total" type="text" value="<?php if($count == 0 ) {echo '1';}else {echo '0';} ?>">
                      <button class="btn plus">+</button>
                    </div>
                  </div>
                </div>

              <?php $count++; endwhile; ?>
            </div>
          </div>

        </div>
      </div>
      <!-- END MARKER TABLETS -->
    <?php endif; ?>


    <?php if( is_singular( 'magniclick_specials' ) ) { $controller = false;?>

      <!-- MAGNICLICK SPECIALS -->
      <div class="board-props" data-current-price="<?php echo esc_attr($main_total_cost); ?>" data-quantity="1">
        <?php if( have_rows('magniclick_special_complect') ) :

          while( have_rows('magniclick_special_complect') ) : the_row(); ?>

          <div class="stick-line" data-stick-line="x<?php echo esc_html(get_sub_field('magniclick_special_complect_total')) . '-' . esc_html( str_replace(' ', '-', get_sub_field('magniclick_special_complect_desc')) );?>">
            <div class="stick-line__img-wrapper">
              <div class="img-block"><img src="<?php echo esc_url(get_sub_field('magniclick_special_complect_preview')); ?>" alt=""></div>
            </div>
            <strong class="stick-line__quantity">
              <span class="stick-line__total">x<?php echo esc_html(get_sub_field('magniclick_special_complect_total'));?></span>
            </strong>
            <span class="stick-line__desc"><?php echo esc_html(get_sub_field('magniclick_special_complect_desc')); ?></span>
          </div>
        <?php endwhile; endif; ?>
      </div>

      <div class="control-wrapper js-productModuleBoard <?php echo esc_attr($active_class); ?>"><strong><span class="control-name">Кол-во:</span></strong>
        <?php $controller_total = 1; ?>
        <?php include(locate_template( 'template-parts/product-board-controller.php' )); ?>
      </div>
      <!-- END MAGNICLICK SPECIALS -->
    <?php } ?>


    <?php if( $controller ) { ?>
      <div class="control-wrapper">
        <?php $controller_total = 1; ?>
        <?php include(locate_template( 'template-parts/product-board-controller.php' )); ?>
      </div>

    <?php } ?>

    <div class="product-cost" data-total-cost="<?php echo esc_attr($main_total_cost); ?>" >
      <span class="rub">&#8381;</span>
    </div>
  <?php endwhile; endif; ?>

  <div class="product-cart-btn-block"><span class="error-log js-erroLog"></span><button class="btn product-cart-btn js-cartBtn">Добавить в корзину</button></div>

  <?php if( $magniclick['main-site-delivery-desc'] ) {?>
    <div class="product-board__info">
      <span><?php echo wp_kses_post( $magniclick['main-site-delivery-desc'] ); ?></span>
    </div>
  <?php } ?>
</div>
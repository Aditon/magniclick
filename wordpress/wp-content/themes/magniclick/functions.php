<?php

# Maniclick functions and definitions
#
# @link https://developer.wordpress.org/themes/basics/theme-functions/
#
# @package Maniclick


if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0.0' );
}

// Function for tests
if( !function_exists('get_result') ) {

  function get_result( $var ) {

    echo "<pre>" . var_export( $var, true ) . "</pre>";
  }
}


$theme_domain = 'magniclick';

if ( ! function_exists( 'magniclick_setup' ) ) :

	# Sets up theme defaults and registers support for various WordPress features.
	#
	# Note that this function is hooked into the after_setup_theme hook, which
	# runs before the init hook. The init hook is too late for some features, such
	# as indicating support for post thumbnails.

	function magniclick_setup() {
    global $theme_domain;
		# Make theme available for translation.
		# Translations can be filed in the /languages/ directory.
		# If you're building a theme based on Maniclick, use a find and replace
		# to change $theme_domain to the name of your theme in all the template files.
		load_theme_textdomain( $theme_domain, get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		# Let WordPress manage the document title.
		# By adding theme support, we declare that this theme does not use a
		# hard-coded <title> tag in the document head, and expect WordPress to
		# provide it for us.
		add_theme_support( 'title-tag' );


		# @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/

		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			array(
				'menu-1' => esc_html__( 'Primary', $theme_domain ),
			)
		);

		# Switch default core markup for search form, comment form, and comments
		# to output valid HTML5.
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			)
		);
	}
endif;
add_action( 'after_setup_theme', 'magniclick_setup' );

# @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar

function magniclick_widgets_init() {
  global $theme_domain;

	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', $theme_domain ),
			'id'            => $theme_domain . '_sidebar',
			'description'   => esc_html__( 'Add widgets here.', $theme_domain ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action( 'widgets_init', 'magniclick_widgets_init' );

# Enqueue scripts and styles.

function magniclick_scripts_styles() {

  #  Styles

	wp_enqueue_style( 'magniclick-style', get_stylesheet_uri(), array(), _S_VERSION );
  wp_enqueue_style( 'fancybox-style', get_template_directory_uri() . '/css/libs/fancybox.min.css', array(), '3.4.0' );
  wp_enqueue_style( 'magniclick-main-style', get_template_directory_uri() . '/css/style.css', array(), _S_VERSION );


  #  Deregister and Register Jquery

  wp_deregister_script( 'jquery' );
  wp_register_script( 'jquery', '//cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js', array(), '3.5.0', true );


  #  Scripts
	wp_enqueue_script( 'magniclick-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), _S_VERSION, true );

  if( is_front_page() or is_archive() ) {
    wp_enqueue_script( 'masonry-grid', get_template_directory_uri() . '/js/libs/masonry-grid.min.js', array( 'jquery' ), '4.2.2', true );
  }

  if( is_front_page() or is_singular() ) {
    wp_enqueue_style( 'owl-carousel-style', get_template_directory_uri() . '/css/libs/owl.carousel.min.css', array(), '2.2.1' );
    wp_enqueue_script( 'owl-carousel-script', get_template_directory_uri() . '/js/libs/owl.carousel.min.js', array( 'jquery' ), '2.2.1', true );
    wp_enqueue_script( 'owl-carousel-thumb', get_template_directory_uri() . '/js/libs/owl.carousel2.thumbs.min.js', array( 'jquery' ), '0.1.8', true );
  }

  if( is_singular( 'magniclick_products' ) or is_singular( 'magniclick_specials' ) or is_page_template( 'template-cart.php' ) ) {
    wp_enqueue_script( 'jquery-cookie-script', '//cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js', array('jquery'), '', true );
  }

  wp_enqueue_script( 'fancybox-script', get_template_directory_uri() . '/js/libs/fancybox.min.js', array('jquery'), '3.4.0', true );
  wp_enqueue_script( 'magniclick-main-script', get_template_directory_uri() . '/js/script.js', array('jquery'), '1.0', true );
}

add_action( 'wp_enqueue_scripts', 'magniclick_scripts_styles' );

# Register Nav Menus
register_nav_menus( array(
  'header-menu'        => esc_html( 'Header Top Menu', $theme_domain ),
  'header-mobile-menu' => esc_html( 'Header Mobile Menu', $theme_domain ),
  'footer-menu'        => esc_html( 'Footer Menu', $theme_domain ),
) );

/*
 Thumbnails
*/
add_image_size( 'main-page-jumb', 770, 560, true );
add_image_size( 'main-page-product-1', '364', '512', true );
add_image_size( 'main-page-product-2', '364', '244', true );
add_image_size( 'more-products', '300', '253', true );
add_image_size( 'product-desc-line', 460, 306, true );
add_image_size( 'search-thumb', 150, 150, true );
add_image_size( 'product-slide-thumb', 73, 73, true );
add_image_size( 'special-thumb', 277, 186, true );
add_image_size( 'module-board', 40, 40, true );
add_image_size( 'cart-preview', 73, 73, true );


# Customizer additions.
require get_template_directory() . '/includes/customizer.php';


# Custom Post Type
require get_template_directory() . '/includes/custom-post-type.php';


# Redux Options
require get_template_directory() . '/includes/redux-main.php';
// require get_template_directory() . '/inc/redux-options.php';

# Custom Menu Walker 
require get_template_directory() . '/includes/custom-menu-walker.php';


function magniclick_function_admin_bar(){
    return false;
}
add_filter( 'show_admin_bar' , 'magniclick_function_admin_bar');

add_filter( 'posts_join', 'magniclick_cf_search_join' );
add_filter( 'posts_where', 'magniclick_cf_search_where' );
add_filter( 'posts_distinct', 'magniclick_cf_search_distinct' );

# Combines record tables and metadata tables
function magniclick_cf_search_join( $join ){
  global $wpdb;

  if( is_search() )
    $join .= " LEFT JOIN $wpdb->postmeta ON ID = $wpdb->postmeta.post_id ";

  return $join;
}

# Specifies which metafields and which value to search for in the WHERE clause
function magniclick_cf_search_where( $where ){
  global $wpdb;

  if ( is_search() ) {
    $where = preg_replace(
      "/\(\s*$wpdb->posts.post_title\s+LIKE\s*(\'[^\']+\')\s*\)/",
      "($wpdb->posts.post_title LIKE $1) OR ($wpdb->postmeta.meta_value LIKE $1)", $where );
  }

  return $where;
}

# Prevents duplicates from appearing in Vyborg
function magniclick_cf_search_distinct( $where ){
  return is_search() ? 'DISTINCT' : $where;
}

function magniclick_set_cart_session() {

  if( isset( $_POST['cart_session'] ) and !empty( $_POST['cart_session'] ) ) {

    $cookie_script = $_POST['cart_session'];


    $cookie_name = 'magniclick_cart_options';
    $cart_total_sum = 0;

    if( $cookie_script ) {
      $cookie = $cookie_script;
      $cookie = stripslashes( $cookie );
      $cookie = preg_replace('/\s+/', '', $cookie);

      $cookie = json_decode( $cookie );
      $cart_array = [];

      foreach ( $cookie as $object ) {
        $object = (array)$object;

        foreach( $object as $key => $value ) {
          if( isset( $value->pageId ) ) {
            $cart_array[$key]['pageId'] = $value->pageId;
          }
          if( isset( $value->orderCovers ) ) {
            $cart_array[$key]['orderCovers'] = $value->orderCovers;
          }
          if( isset( $value->orderSet ) ) {
            $cart_array[$key]['orderSet'] = $value->orderSet;
          }
          if( isset( $value->orderColors ) ) {
            $cart_array[$key]['orderColors'] = $value->orderColors;
          }
          if( isset( $value->specialCards ) ) {
            $cart_array[$key]['specialCards'] = $value->specialCards;
          }
          if( isset( $value->orderQuantity ) ) {
            $cart_array[$key]['orderQuantity'] = $value->orderQuantity;
          }
          if( isset( $value->orderTotalCost ) ) {
            $cart_array[$key]['orderTotalCost'] = $value->orderTotalCost;
            $cart_total_sum += (int)$value->orderTotalCost;
          }
        }

      }

    } ?>


    <div class="cart-page__col-top">
      <h1 class="block-title-md">Ваш заказ</h1>
      <div class="cart-list">

        <?php
          if( !empty( $cart_array ) ) {

            foreach( $cart_array as $key => $value ) {

              $order_covers = isset($value['orderCovers']) ? str_replace([';'], ', ', $value['orderCovers']) : null;
              $order_set = isset($value['orderSet']) ? 'Набор: ' . preg_replace("/[^0-9]/", '', $value['orderSet']) : null;
              $order_cards = isset($value['specialCards']) ? str_replace(['x', ';'], ['', ','], $value['specialCards']) : null;
              $order_colors = isset($value['orderColors']) ? explode( ';', $value['orderColors'] ) : null;
              $color_preset = '';
              $order_current_desc = '';

              if( $order_colors ) {
                foreach ( $order_colors as $color ) {
                  $temp_color = explode( '-' , $color);
                  $color_preset .= "<span class='cart-list__item-color-preset' data-color-preset='x{$temp_color[1]}' style='background-color: {$temp_color[0]}'></span>";
                }
              }

              if( $order_covers ) { $order_current_desc .= ' ' . $order_covers; }
              if( $order_set ) { $order_current_desc .= ' ' . $order_set; }
              if( $order_cards ) { $order_current_desc .= ' ' . $order_cards; }
              if( $order_colors ) { $order_current_desc .= ' ' . $color_preset; }

            ?>


              <div class="cart-list__item item js-cartListItem" data-object-id="<?php echo esc_attr($value['pageId']); ?>" data-object-key="<?php echo esc_attr($key); ?>">
                <div class="row align-items-center">
                  <div class="col cart-list__item-preview">
                    <div class="img-block"><img src="<?php echo esc_url( get_the_post_thumbnail_url( $value['pageId'], 'cart-preview' ) ); ?>" alt="<?php echo esc_attr( get_the_title( $value['pageId'] ) ); ?>"></div>
                  </div>

                  <div class="col cart-list__item-main-desc">
                    <div class="cart-list__item-name"><strong><span><?php echo esc_attr( get_the_title( $value['pageId'] ) ); ?></span></strong></div>
                    <div class="cart-list__item-desc"><span><?php if( !empty( $order_current_desc ) ) { echo wp_kses_post( $order_current_desc ); } ?></span></div>
                  </div>

                  <div class="col cart-list__item-cost"><strong class="total" data-object-cost="<?php echo esc_attr( ($value['orderTotalCost'] == 0 ? 1 : $value['orderTotalCost'])/$value['orderQuantity']  ); ?>" data-temp-cost="<?php echo esc_attr( $value['orderTotalCost'] == 0 ? 1 : $value['orderTotalCost']  ); ?>"></strong><span class="rub">&#8381;</span></div>
                  <div class="col cart-list__item-control">
                    <div class="number-control js-numberControl" data-quantity="<?php echo esc_attr( $value['orderQuantity'] ); ?>">
                      <button class="btn minus">&#8722;</button>
                      <input class="total" type="text" value="<?php echo esc_attr( $value['orderQuantity'] ); ?>">
                      <button class="btn plus">+</button>
                    </div>
                  </div>

                  <?php get_template_part( 'template-parts/cart/cart', 'close-elem' ); ?>

                </div>
              </div>
            <?php }
          }
        ?>

      </div>
      <div class="cart-total-info">
        <div class="cart-total-info__line">
          <table>

            <tr>
              <td><strong>Всего к оплате:</strong></td>
              <td class="total" data-total-price="<?php echo esc_attr( $cart_total_sum ); ?>"> <span class="rub">&#8381;</span></td>
            </tr>
          </table>
        </div>
      </div>
    </div>

  <?php }
  wp_die();

}

function magniclick_session_var(){

  wp_localize_script( 'magniclick-main-script', 'cartAjaxUrl', 
    array(
      'url' => admin_url('admin-ajax.php')
    )
  );  
}

/*
* Home category admin ajax register
*/
add_action( 'wp_enqueue_scripts', 'magniclick_session_var');
add_action('wp_ajax_magniclick_set_cart_session', 'magniclick_set_cart_session');
add_action('wp_ajax_nopriv_magniclick_set_cart_session', 'magniclick_set_cart_session');
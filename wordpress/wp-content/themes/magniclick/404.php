<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package Maniclick
 */

get_header();
?>

	<main id="primary" class="site-main main-content">
    <div class="container">

  		<section class="error-404 not-found text-center main-font">
  			<header class="page-header">
  				<h1 class="page-title main-font"><?php esc_html_e( '404', 'magniclick_' ); ?></h1>
  			</header><!-- .page-header -->

        <p>Страница не найдена <br> Вернуться на <a href="<?php echo esc_url( site_url( '/' ) ); ?>">главную</a></p>

        
  		</section><!-- .error-404 -->
    </div>

	</main><!-- #main -->

<?php
get_footer();

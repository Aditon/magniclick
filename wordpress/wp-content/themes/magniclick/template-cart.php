<?php
  /*
    Template Name: Cart Template
  */
  get_header();

  global $magniclick;

?>

<div class="main-content">
  <div class="cart-page-wrapper main-font" id="cart-page" <?php $empty_cart_img = null; if( $magniclick['empty-cart-img']['url'] ) { $empty_cart_img = esc_url($magniclick['empty-cart-img']['url']); echo "data-empty-cart-img-url={$empty_cart_img}"; } ?>>
    <div class="container">

      <div class="row">
        <div class="col-8 cart-main-col" id="cart-content">
        </div>
        <div class="col-3 cart-form-col js-cartFormCol hidden">
          <div class="cart-form">
            <p>Чтобы оформить заказ, оставьте свои контакты и с вами свяжется менеджер</p>
              <?php echo do_shortcode( '[contact-form-7 id="370" title="Cart Form"]' ); ?>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>


<?php get_footer(); ?>
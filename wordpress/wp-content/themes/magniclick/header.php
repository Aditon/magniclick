<?php
/**
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Maniclick
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<?php global $magniclick; ?>
  <head>
  	<meta charset="<?php bloginfo( 'charset' ); ?>">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Description">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <meta name="format-detection" content="telephone=no">
    <?php if($magniclick['site-favicon']['url']) {?>
      <link rel="icon" type="image/png" href="<?php echo esc_url( $magniclick['site-favicon']['url'] ) ?>" />
    <?php } ?>
  	<?php wp_head(); ?>
  </head>


  <body <?php $classes = ''; if( is_front_page() ) { $classes .= ' main-page'; }elseif( is_page_template( 'about-us.php' ) ){ $classes .= 'about-us-page'; } body_class( $classes ); ?> >

    <?php wp_body_open(); ?>

    <div class="container-fluid">
      <header class="header">
        <div class="top-bar js-topBar main-font">
          <div class="main-container">
            <div class="row align-items-center">

              <div class="col">
                <button class="nav-toggle js-navToggle"><span></span><span></span><span></span></button>

                <a class="site-top-logo" href="<?php echo esc_url(site_url( '/')); ?>" title="<?php echo esc_html(bloginfo( 'site_name' )); ?>">
                  <?php if($magniclick['header-logo']['url']) {?>
                  <img src="<?php echo esc_url($magniclick['header-logo']['url']); ?>" alt="<?php echo esc_html(bloginfo( 'site_name' )); ?>">
                  <?php } else { echo esc_html(bloginfo( 'site_name' )); }?>
                </a>
              </div>

              <div class="col nav-col">
                <div class="row align-items-center">
                  <?php if( has_nav_menu( 'header-menu' ) ) {?>
                    <div class="col">
                      <nav class="site-top-navigation">
                        <?php
                          wp_nav_menu( [
                            'theme_location'  => 'header-menu',
                            'menu'            => '', 
                            'container'       => 'ul', 
                            'container_class' => 'menu'
                          ] );
                        ?>
                      </nav>
                    </div>
                  <?php } ?>

                  <div class="col search-col">
                    <?php get_search_form(); ?>
                  </div>

                  <!-- <div class="col"><a id="cart" class="cart<?php #if( !get_cart_total() ) { echo ' empty-cart'; } ?>" href="<?php #echo esc_url( get_the_permalink( 368 ) ? get_the_permalink( 368 ) : get_the_permalink( 291 ) ) ?>"><span class="icon cart-icon"><span class="cart-total"><?php #if(get_cart_total()) {echo esc_html(get_cart_total());} ?></span></span></a></div> -->

                  <div class="col"><a id="cart" class="cart empty-cart" href="<?php echo esc_url( get_the_permalink( 368 ) ? get_the_permalink( 368 ) : get_the_permalink( 291 ) ) ?>"><span class="icon cart-icon"><span class="cart-total"></span></span></a></div>

                </div>
              </div>

            </div>
          </div>
        </div>

        <div class="mobile-menu js-mobileMenu">
          <div class="mobile-menu-inner">
            <div class="mobile-menu-search search-col">
              <form role="search" method="get" id="searchform-mobile" action="<?php echo home_url( '/' ) ?>" >
                <i class="icon search-icon">
                  <input type="submit" id="searchsubmit-mobile" value="">
                </i>
                <input class="search-input" type="search" value="<?php echo get_search_query() ?>" name="s" />
              </form>

            </div>

            <?php if( has_nav_menu( 'header-mobile-menu' ) ) {?>
              <nav class="site-mobile-navigation">
                  <?php
                    wp_nav_menu( [
                      'theme_location'  => 'header-mobile-menu',
                      'menu'            => '', 
                      'container'       => 'ul', 
                      'container_class' => 'menu',
                      'walker'          =>  new Nav_Walker_Nav_Menu()
                    ] );
                  ?>
              </nav>
            <?php } ?>

          </div>
        </div>
      </header>
<?php
/**

 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Maniclick
 */

  global $magniclick;
?>
      <div class="container">
        <?php #if( is_front_page() ) {echo do_shortcode( '[jr_instagram id="2"]' );}; ?>
      </div>
      <footer class="footer main-font">
        <div class="container">
          <div class="row row--first">
            <div class="col-3">
              <a class="site-logo footer__logo" href="<?php echo esc_url( site_url( '/' ) ); ?>">
                <?php if( $magniclick['footer-logo']['url'] ) {?>
                  <img src="<?php echo esc_url( $magniclick['footer-logo']['url'] ); ?>" alt="<?php echo esc_html( bloginfo( 'site_name' ) ); ?>">
                <?php }else{ echo esc_html( bloginfo( 'site_name' ) ); } ?>
              </a>
              <nav class="footer__menu">
                <?php
                  wp_nav_menu( [
                    'theme_location'  => 'footer-menu',
                    'menu'            => '', 
                    'container'       => 'ul'
                  ] );
                ?>
              </nav>
            </div>

            <div class="col-3">
              <h3 class="footer__title">Наши контакты</h3>
              <div class="footer__contacts">

                <?php if( $magniclick['phone-number'] ) {?>
                  <div class="site-phone footer__contact-item">
                    <a href="tel:<?php echo esc_html( str_replace( [ ' ', '-', '(', ')' ], '', $magniclick['phone-number']) ); ?>">
                      <i class="icon phone-icon-white" aria-hidden="true"></i>
                      <?php echo esc_html($magniclick['phone-number']); ?>
                    </a>
                  </div>
                <?php } ?>

                <?php if( $magniclick['mail-address'] ) {?>
                  <div class="site-email footer__contact-item">
                    <a href="mailto:<?php echo esc_html($magniclick['mail-address']); ?>">
                      <i class="icon mail-icon-white" aria-hidden="true"></i>
                      <?php echo esc_html($magniclick['mail-address']); ?>
                    </a>
                  </div>
                <?php } ?>

                <?php if( $magniclick['viber-link'] or $magniclick['telegram-link'] or $magniclick['whatsapp-link'] ) {?>
                  <div class="social-block footer__social-block footer__contact-item">

                    <?php if( $magniclick['viber-link'] ) {?>
                      <a class="social-link" href="<?php echo esc_url($magniclick['viber-link']); ?>">
                        <i class="icon viber-icon-white" aria-hidden="true"></i>
                      </a>
                    <?php } ?>

                    <?php if( $magniclick['telegram-link'] ) {?>
                      <a class="social-link" href="<?php echo esc_url($magniclick['telegram-link']); ?>">
                        <i class="icon telegram-icon-white" aria-hidden="true"></i>
                      </a>
                    <?php } ?>

                    <?php if( $magniclick['whatsapp-link'] ) {?>
                      <a class="social-link" href="<?php echo esc_url($magniclick['whatsapp-link']); ?>">
                        <i class="icon whatsapp-icon-white" aria-hidden="true"></i>
                      </a>
                    <?php } ?>
                  </div>
                <?php } ?>

              </div>
            </div>

            <div class="col-6">
              <div class="row">
                <div class="col-6">
                  <h3 class="footer__title">Контакты для связи</h3>
                </div>
              </div>

              <div class="footer__form white-form">
                <?php echo do_shortcode( '[contact-form-7 id="334" title="Footer Form"]' ) ?>
              </div>

            </div>
          </div>
        </div>
        <?php get_template_part( 'template-parts/contact-popup' ); ?>
      </footer>

      <div class="arrow-up js-arrowUp"></div>

    </div><!-- #page -->

    <div class="hidden" hidden>    
      <div id="send-modal">
        <div class="img-block">
          <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
             viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
            <g>
              <path d="M437.019,74.98C388.667,26.629,324.38,0,256,0C187.619,0,123.331,26.629,74.98,74.98C26.628,123.332,0,187.62,0,256
                s26.628,132.667,74.98,181.019C123.332,485.371,187.619,512,256,512c68.38,0,132.667-26.629,181.019-74.981
                C485.371,388.667,512,324.38,512,256S485.371,123.333,437.019,74.98z M256,482C131.383,482,30,380.617,30,256S131.383,30,256,30
                s226,101.383,226,226S380.617,482,256,482z"/>
            </g>
            <g>
              <path d="M378.305,173.859c-5.857-5.856-15.355-5.856-21.212,0.001L224.634,306.319l-69.727-69.727
                c-5.857-5.857-15.355-5.857-21.213,0c-5.858,5.857-5.858,15.355,0,21.213l80.333,80.333c2.929,2.929,6.768,4.393,10.606,4.393
                c3.838,0,7.678-1.465,10.606-4.393l143.066-143.066C384.163,189.215,384.163,179.717,378.305,173.859z"/>
            </g>
          </svg>

        </div>
      </div>
    </div>

    <?php if( is_page_template( 'template-cart.php' ) ) {?>
      <!-- Cart Popup -->
      <div id="send-cart-modal" class="cart-popup hidden" hidden="hidden">
        <button class="cart-popup__close js-closeCartPopup"><svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M13.4099 11.9999L19.7099 5.70994C19.8982 5.52164 20.004 5.26624 20.004 4.99994C20.004 4.73364 19.8982 4.47825 19.7099 4.28994C19.5216 4.10164 19.2662 3.99585 18.9999 3.99585C18.7336 3.99585 18.4782 4.10164 18.2899 4.28994L11.9999 10.5899L5.70994 4.28994C5.52164 4.10164 5.26624 3.99585 4.99994 3.99585C4.73364 3.99585 4.47824 4.10164 4.28994 4.28994C4.10164 4.47825 3.99585 4.73364 3.99585 4.99994C3.99585 5.26624 4.10164 5.52164 4.28994 5.70994L10.5899 11.9999L4.28994 18.2899C4.19621 18.3829 4.12182 18.4935 4.07105 18.6154C4.02028 18.7372 3.99414 18.8679 3.99414 18.9999C3.99414 19.132 4.02028 19.2627 4.07105 19.3845C4.12182 19.5064 4.19621 19.617 4.28994 19.7099C4.3829 19.8037 4.4935 19.8781 4.61536 19.9288C4.73722 19.9796 4.86793 20.0057 4.99994 20.0057C5.13195 20.0057 5.26266 19.9796 5.38452 19.9288C5.50638 19.8781 5.61698 19.8037 5.70994 19.7099L11.9999 13.4099L18.2899 19.7099C18.3829 19.8037 18.4935 19.8781 18.6154 19.9288C18.7372 19.9796 18.8679 20.0057 18.9999 20.0057C19.132 20.0057 19.2627 19.9796 19.3845 19.9288C19.5064 19.8781 19.617 19.8037 19.7099 19.7099C19.8037 19.617 19.8781 19.5064 19.9288 19.3845C19.9796 19.2627 20.0057 19.132 20.0057 18.9999C20.0057 18.8679 19.9796 18.7372 19.9288 18.6154C19.8781 18.4935 19.8037 18.3829 19.7099 18.2899L13.4099 11.9999Z" fill="#354151"/>
            </svg>
        </button>

        <div class="cart-popup__inner main-font">
          <div class="block-title-md"><span>Заказ принят!</span></div>
          <p>Ваш заказ уже пакуют, в&nbsp;ближайшее время с&nbsp;вами свяжется менеджер для уточнения деталей</p>
          <?php if( $magniclick['cart-thanks-icon']['url'] ) {?>
            <div class="img-block"><img src="<?php echo esc_url($magniclick['cart-thanks-icon']['url']); ?>" alt="Cart Popup"></div>
          <?php } ?>
        </div>
      </div>
    <?php } ?>

    <?php wp_footer(); ?>
  </body>
</html>

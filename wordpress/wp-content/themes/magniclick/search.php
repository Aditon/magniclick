<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package Maniclick
 */

get_header();
?>

  <div class="main-content">
    <div class="container">
      <main id="primary" class="site-main search-page">

        <?php if ( have_posts() ) : ?>

          <header class="page-header">
            <h1 class="block-title block-title-md page-title">
              <?php
              /* translators: %s: search query. */
              printf( esc_html__( 'Результат поиска: %s', 'magniclick_' ), '<span>' . get_search_query() . '</span>' );
              ?>
            </h1>
          </header><!-- .page-header -->

          <div class="row">
            <?php
            /* Start the Loop */
            while ( have_posts() ) :
              the_post();

              get_template_part( 'template-parts/content', 'search' );

            endwhile; ?>
          </div>
        <?php
        else :

          get_template_part( 'template-parts/content', 'none' );

        endif;
        ?>

      </main><!-- #main -->
    </div>
  </div>
<?php

get_footer();

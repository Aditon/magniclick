<?php 

  /*
    Template Name: FAQ Template
  */

  get_header();
?>

<div class="faq-page">
  <main class="main-content sec">
    <div class="container">

      <h1><?php echo esc_html( get_the_title() ); ?></h1>

      <div class="row">

        <?php if( have_rows( 'magniclick_faq_block' ) ) : ?>

          <?php while( have_rows( 'magniclick_faq_block' ) ) : the_row(); ?>

            <div class="col-6">
              <div class="faq-line">
                <button class="btn faq-line__question js-faqLine"><?php echo esc_html( get_sub_field('magniclick_faq_block_question') ); ?></button>
                <div class="faq-line__answer hidden js-faqLineAnswer">
                  <p><?php echo esc_html(get_sub_field('magniclick_faq_block_answer')); ?></p>
                </div>
              </div>
            </div>
          <?php endwhile;endif;
        ?>
      </div>

      <p class="faq-support">Не нашли ответ на ваш вопрос? Обратитесь в <a href="#" class="js-openContacts">поддержку</a></p>
    </div>
  </main>
</div>

<?php get_footer(); ?>
<?php

  get_header();
?>

  <?php get_template_part( 'template-parts/content', 'jumb' ) ?>
  
  <?php get_template_part( 'template-parts/content', 'desc-line' ) ?>

  <?php get_template_part( 'template-parts/product', 'advantages' ) ?>

  <?php get_template_part( 'template-parts/product', 'montage' ) ?>

  <?php get_template_part( 'template-parts/content', 'more-products' ) ?>

  <?php get_template_part('template-parts/content', 'specials'); ?>

<?php
  get_footer();
?>
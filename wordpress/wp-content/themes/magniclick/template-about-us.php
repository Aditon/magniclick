<?php

  /*
    Template Name: About Us
  */

  get_header();
?>

<section class="about-us main-font">
  <div class="main-container">
    <div class="container">
      <div class="about-us__main-desc">

        <?php if( have_posts() ) : while( have_posts() ) : the_post(); ?>
          <h1><?php echo wp_kses_post( get_the_title() ); ?></h1>

          <?php echo wp_kses_post( get_the_content() ); ?>
        <?php endwhile; endif; ?>


        <?php $postulat_gallery = get_field('magniclick_postulat_gallery'); if( $postulat_gallery ) {?>
          <div class="postulat">
            <div class="img-block">
              <?php foreach( $postulat_gallery as $postulat_img ) { ?>
                  <img src="<?php echo esc_url( $postulat_img ); ?>" alt="">
              <?php } ?>
            </div>
          </div>
        <?php } ?>
      </div>
    </div>

    <?php $main_gallery = get_field('magniclick_about_us_gallery'); if( $main_gallery ) {?>
      <div class="img-block about-us__img-container">
        <?php foreach( $main_gallery as $img ) { ?>
          <img src="<?php echo esc_url( $img ); ?>" alt="">
        <?php } ?>
      </div>
    <?php } ?>

  </div>
</section>

<?php
  get_footer();
?>
<?php

get_header();
?>

<!-- Products -->
<section class="products-block sec">
  <div class="container">
    <h1><?php esc_html_e( 'Для кого', 'magniclick' ); ?></h1>

    <?php
      $args = array(
        'post_type' => 'magniclick_pages',
        'posts_per_page' => -1,
        'order' => 'asc'
      );

      $products = new WP_Query( $args );
    ?>

    <?php get_template_part( 'template-parts/archive', 'content' ) ?>

  </div>
</section>

<?php
get_footer();

<?php
  /**
   * Registers a new post type
   * @uses $wp_post_types Inserts new post type object into the list
   *
   * @param string  Post type key, must not exceed 20 characters
   * @param array|string  See optional args description above.
   * @return object|WP_Error the registered post type object, or an error object
   */
  $theme_domain = 'magniclick';

  function magniclick_post_types() {
    global $theme_domain;
  
    register_post_type( 'magniclick_products', array(
      'labels' => array(
        'name'               => __( 'Products', $theme_domain ),
        'singular_name'      => __( 'Product', $theme_domain ),
        'add_new'            => __( 'Add New Product', $theme_domain ),
        'add_new_item'       => __( 'Add New Product item', $theme_domain ),
        'edit_item'          => __( 'Edit Product Item', $theme_domain ),
        'new_item'           => __( 'New Product Item', $theme_domain ),
        'view_item'          => __( 'View Product Item', $theme_domain ),
        'search_items'       => __( 'Search Product', $theme_domain ),
        'not_found'          => __( 'Nothing found Products', $theme_domain ),
        'not_found_in_trash' => __( 'Nothing found Products in Trash', $theme_domain ),
        'menu_name'          => __( 'Products', $theme_domain ),
      ),
      'public'              => true,
      'show_in_rest'        => true,
      'menu_position'       => 5,
      'menu_icon'           => 'dashicons-book-alt',
      'supports'            => array( 'title', 'thumbnail', ),
      'has_archive'         => true,
      'rewrite'             => array(
        'slug'            => 'product',
        'with_front'      => false,
        'feeds'           => false,
      ),
    ) );

    register_post_type( 'magniclick_specials', array(
      'labels' => array(
        'name'               => __( 'Specials', $theme_domain ),
        'singular_name'      => __( 'Special', $theme_domain ),
        'add_new'            => __( 'Add New Special', $theme_domain ),
        'add_new_item'       => __( 'Add New Special item', $theme_domain ),
        'edit_item'          => __( 'Edit Special Item', $theme_domain ),
        'new_item'           => __( 'New Special Item', $theme_domain ),
        'view_item'          => __( 'View Special Item', $theme_domain ),
        'search_items'       => __( 'Search Special', $theme_domain ),
        'not_found'          => __( 'Nothing found Specials', $theme_domain ),
        'not_found_in_trash' => __( 'Nothing found Specials in Trash', $theme_domain ),
        'menu_name'          => __( 'Special', $theme_domain ),
      ),
      'public'              => true,
      'show_in_rest'        => true,
      'menu_position'       => 6,
      'menu_icon'           => 'dashicons-book-alt',
      'supports'            => array( 'title', 'thumbnail', ),
      'has_archive'         => true,
      'rewrite'             => array(
        'slug'            => 'special',
        'with_front'      => false,
        'feeds'           => false,
      ),
    ) );

    register_post_type( 'magniclick_pages', array(
      'labels' => array(
        'name'               => __( 'Magniclick Pages', $theme_domain ),
        'singular_name'      => __( 'Magniclick Page', $theme_domain ),
        'add_new'            => __( 'Add New Magniclick Page', $theme_domain ),
        'add_new_item'       => __( 'Add New Magniclick Page item', $theme_domain ),
        'edit_item'          => __( 'Edit Magniclick Page Item', $theme_domain ),
        'new_item'           => __( 'New Magniclick Page Item', $theme_domain ),
        'view_item'          => __( 'View Magniclick Page Item', $theme_domain ),
        'search_items'       => __( 'Search Magniclick Page', $theme_domain ),
        'not_found'          => __( 'Nothing found Magniclick Pages', $theme_domain ),
        'not_found_in_trash' => __( 'Nothing found Magniclick Pages in Trash', $theme_domain ),
        'menu_name'          => __( 'Magniclick Page', $theme_domain ),
      ),
      'public'              => true,
      'show_in_rest'        => true,
      'show_in_nav_menus'   => true,
      'menu_position'       => 7,
      'menu_icon'           => 'dashicons-book-alt',
      'supports'            => array( 'title', 'editor', 'thumbnail', ),
      'has_archive'         => true,
      'rewrite'             => array(
        'slug'            => 'pages',
        'with_front'      => false,
        'feeds'           => false,
      ),
    ) );


    // Register taxonomy
    register_taxonomy( 'module_board', [ 'magniclick_products' ], [ 
      'label'                 => '', // определяется параметром $labels->name
      'labels'                => [
        'name'              => 'Boards',
        'singular_name'     => 'Board',
        'search_items'      => 'Search Board',
        'all_items'         => 'All Boards',
        'add_new_item'      => 'Add New Board',
        'new_item_name'     => 'New Board',
        'menu_name'         => 'Boards',
      ],
      'public'                => true,
      'publicly_queryable'    => null,
      'show_in_nav_menus'     => false,
      'show_in_menu'          => true,
      'show_in_quick_edit'    => false,
      'hierarchical'          => true,

      'rewrite'               => false,
    ] );
  }
  
  add_action( 'init', 'magniclick_post_types' );

  if( is_archive() ) {
      $wp_query->set_404();
      status_header( 404 );
      nocache_headers();
      include("404.php");
      die;
  }

?>
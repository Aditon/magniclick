<?php

// -> Header Main Options
Redux::setSection( $opt_name, array(
    'title'            => esc_html( 'Main Site Options', $opt_name ),
    'id'               => 'main-site-options',
    'desc'             => esc_html( 'Основные настроки сайта', $opt_name ),
    'customizer_width' => '400px',
    'icon'             => 'el el-home'
) );

    Redux::setSection( $opt_name, array(
        'title'      => esc_html( 'Delivery', $opt_name ),
        'id'         => 'main-site-delivery',
        'subsection'       => true,
        'desc'       => esc_html( 'Информация о доставке', $opt_name ),
        'fields'     => array(
            array(
                'id'       => 'main-site-delivery-desc',
                'type'     => 'text',
                'title'      => esc_html( 'Текст', $opt_name ),
                'desc'       => '',
                'default'  => 'Доставка 2&nbsp;-&nbsp;15&nbsp;раб. дней. Бесплатная доставка при&nbsp;покупке от&nbsp;2000&nbsp;руб.',
            ),
            array(
                'id'       => 'site-favicon',
                'type'     => 'media',
                'url'      => true,
                'compiler' => 'true',
                'title'      => esc_html( 'Site Favicon', $opt_name ),
                'desc'       => esc_html( 'Загрузите ваш favicon для сайта', $opt_name ),
                'default'  => '',
            ),
            array(
                'id'       => 'cart-thanks-icon',
                'type'     => 'media',
                'url'      => true,
                'compiler' => 'true',
                'title'      => esc_html( 'Cart Success Image', $opt_name ),
                'desc'       => esc_html( 'Загрузите вашу картинку для popup успешной отправки формы на странице корзины', $opt_name ),
                'default'  => '',
            ),
            array(
                'id'       => 'empty-cart-img',
                'type'     => 'media',
                'url'      => true,
                'compiler' => 'true',
                'title'      => esc_html( 'Empty Cart Image', $opt_name ),
                'desc'       => esc_html( 'Загрузите вашу картинку пустой страницы корзины', $opt_name ),
                'default'  => '',
            ),
        ),
    ) );

// -> Header Main Options
Redux::setSection( $opt_name, array(
    'title'            => esc_html( 'Header Main Options', $opt_name ),
    'id'               => 'header-main-options',
    'desc'             => esc_html( 'Основные настроки шапки сайта', $opt_name ),
    'customizer_width' => '400px',
    'icon'             => 'el el-home'
) );

    Redux::setSection( $opt_name, array(
        'title'      => esc_html( 'Header Top Bar', $opt_name ),
        'id'         => 'header-top-bar',
        'subsection'       => true,
        'desc'       => esc_html( 'Предпочтительны файлы с расширением .svg', $opt_name ),
        'fields'     => array(
            array(
                'id'       => 'header-logo',
                'type'     => 'media',
                'url'      => true,
                'compiler' => 'true',
                'title'      => esc_html( 'Header Logo Icon', $opt_name ),
                'desc'       => esc_html( 'Загрузите ваше лого в header', $opt_name ),
                'default'  => '',
            ),
        ),
    ) );

  // -> Typography
  Redux::setSection( $opt_name, array(
      'title'  => esc_html( 'Typography', $opt_name ),
      'id'     => 'main-typography',
      'desc'   => '',
      'icon'   => 'el el-font',
      'fields' => array(
          array(
              'id'       => 'body-typography',
              'type'     => 'typography',
              'title'    => esc_html( 'Body Typography', $opt_name ),
              'subtitle' => esc_html( 'Настройки тела документа', $opt_name ),
              'output' => array('body'),
              'units'  => '%',
              'default'  => array(
                'font-family'  => 'Montserrat, Helvetica, serif',
                'font-size'    => '90',
                'line-height'  => '200',
                'font-weight'  => '400',
                'color'        => '#575568',
              ),
          ),
          array(
              'id'       => 'main-font',
              'type'     => 'typography',
              'title'    => esc_html( 'Main Font', $opt_name ),
              'subtitle' => esc_html( 'Оснойвной шрифт', $opt_name ),
              'google'   => true,
              'output' => array('.main-font'),
              'default'  => array(
                  'font-family' => 'Montserrat',
              ),
          ),
          array(
              'id'          => 'title-font',
              'type'        => 'typography',
              'title'    => esc_html( 'Title Font', $opt_name ),
              'subtitle' => esc_html( 'Шрифт заголовков', $opt_name ),
              'google'   => true,
              'output' => array('.second-font'),
              'default'  => array(
                  'font-family' => 'Merriweather',
              ),
          )
      )
  ) );

// -> Footer Main Options
Redux::setSection( $opt_name, array(
    'title'            => esc_html( 'Footer Main Options', $opt_name ),
    'id'               => 'footer-main-options',
    'desc'             => esc_html( 'Основные настройки подвала сайта', $opt_name ),
    'customizer_width' => '400px',
    'icon'             => 'el el-home'
) );

    Redux::setSection( $opt_name, array(
        'title'      => esc_html( 'Footer', $opt_name ),
        'id'         => 'main-footer',
        'subsection'       => true,
        'desc'       => esc_html( 'Предпочтительны файлы с расширением .svg', $opt_name ),
        'fields'     => array(
            array(
                'id'       => 'footer-logo',
                'type'     => 'media',
                'url'      => true,
                'compiler' => 'true',
                'title'      => esc_html( 'Footer Logo Icon', $opt_name ),
                'desc'       => esc_html( 'Загрузите ваше лого в footer', $opt_name ),
                'default'  => '',
            ),
            array(
                'id'       => 'footer-back',
                'type'     => 'background',
                'output'   => array( '.footer' ),
                'title'    => esc_html( 'Footer Background', $opt_name ),
                'subtitle' => esc_html( 'Цвет бэкраунда footer', $opt_name ),
                'default'  => '#312783',
            ),
            array(
                'id'       => 'footer-color',
                'type'     => 'typography',
                'title'    => esc_html( 'Footer Color', $opt_name ),
                'subtitle' => esc_html( 'Цвет footer', $opt_name ),
                'google'   => true,
                'output'   => array( '.footer' ),
                'default'  => array(
                  'color' => 'white',
                ),
            ),
        ),
    ) );

// -> Footer Main Options
Redux::setSection( $opt_name, array(
    'title'            => esc_html( 'Site Contacts Options', $opt_name ),
    'id'               => 'site-contacts-options',
    'desc'             => esc_html( 'Контакты сайта', $opt_name ),
    'customizer_width' => '400px',
    'icon'             => 'el el-list-alt'
) );

    Redux::setSection( $opt_name, array(
        'title'      => esc_html( 'Контакты', $opt_name ),
        'id'         => 'site-contacts',
        'subsection'       => true,
        'fields'     => array(
            array(
                'id'       => 'phone-number',
                'type'     => 'text',
                'title'    => esc_html( 'Phone Number', $opt_name ),
                'subtitle' => esc_html( 'Введите телефон', $opt_name ),
                'default'  => '+ 7 900 567 89 54',
            ),

            array(
                'id'       => 'mail-address',
                'type'     => 'text',
                'title'    => esc_html( 'Mail address', $opt_name ),
                'subtitle' => esc_html( 'Введите адрес почты', $opt_name ),
                'default'  => 'Magniclick@gmail.ru',
            ),

            array(
                'id'       => 'viber-link',
                'type'     => 'text',
                'title'    => esc_html( 'Viber link', $opt_name ),
                'subtitle' => esc_html( 'Введите вашу ссылку viber', $opt_name ),
                'default'  => '',
            ),

            array(
                'id'       => 'telegram-link',
                'type'     => 'text',
                'title'    => esc_html( 'Telegram link', $opt_name ),
                'subtitle' => esc_html( 'Введите вашу ссылку telegram', $opt_name ),
                'default'  => '',
            ),

            array(
                'id'       => 'whatsapp-link',
                'type'     => 'text',
                'title'    => esc_html( 'Whatsapp link', $opt_name ),
                'subtitle' => esc_html( 'Введите вашу ссылку whatsapp', $opt_name ),
                'default'  => '',
            ),

            array(
                'id'       => 'time-desc',
                'type'     => 'text',
                'title'    => esc_html( 'Time description', $opt_name ),
                'subtitle' => esc_html( 'Введите ваше время работы', $opt_name ),
                'default'  => '9.00 — 18.00',
            ),
        ),
    ) );


?>
<?php
#
# Maniclick Theme Customizer
#
# @package Maniclick
#

#
# Add postMessage support for site title and description for the Theme Customizer.
#
# @param WP_Customize_Manager $wp_customize Theme Customizer object.
#
function magniclick__customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';

	if ( isset( $wp_customize->selective_refresh ) ) {
		$wp_customize->selective_refresh->add_partial(
			'blogname',
			array(
				'selector'        => '.site-title a',
				'render_callback' => 'magniclick__customize_partial_blogname',
			)
		);
		$wp_customize->selective_refresh->add_partial(
			'blogdescription',
			array(
				'selector'        => '.site-description',
				'render_callback' => 'magniclick__customize_partial_blogdescription',
			)
		);
	}
}
add_action( 'customize_register', 'magniclick__customize_register' );

#
# Render the site title for the selective refresh partial.
#
# @return void
#
function magniclick__customize_partial_blogname() {
	bloginfo( 'name' );
}

#
# Render the site tagline for the selective refresh partial.
#
# @return void
#
function magniclick__customize_partial_blogdescription() {
	bloginfo( 'description' );
}

#
# Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
#
function magniclick__customize_preview_js() {
	wp_enqueue_script( 'magniclick_-customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '20151215', true );
}
add_action( 'customize_preview_init', 'magniclick__customize_preview_js' );

/**
 * Add support upload extra file types
 */
add_filter( 'upload_mimes', 'magniclick_svg_upload_allow' );

# Добавляет SVG в список разрешенных для загрузки файлов.
function magniclick_svg_upload_allow( $mimes ) {
  $mimes['svg']  = 'image/svg+xml';

  return $mimes;
}

add_filter( 'wp_check_filetype_and_ext', 'magniclick_fix_svg_mime_type', 10, 5 );

# Исправление MIME типа для SVG файлов.
function magniclick_fix_svg_mime_type( $data, $file, $filename, $mimes, $real_mime = '' ){

  // WP 5.1 +
  if( version_compare( $GLOBALS['wp_version'], '5.1.0', '>=' ) )
    $dosvg = in_array( $real_mime, [ 'image/svg', 'image/svg+xml' ] );
  else
    $dosvg = ( '.svg' === strtolower( substr($filename, -4) ) );

  // mime тип был обнулен, поправим его
  // а также проверим право пользователя
  if( $dosvg ){

    // разрешим
    if( current_user_can('manage_options') ){

      $data['ext']  = 'svg';
      $data['type'] = 'image/svg+xml';
    }
    // запретим
    else {
      $data['ext'] = $type_and_ext['type'] = false;
    }

  }

  return $data;
}

add_filter( 'wp_prepare_attachment_for_js', 'magniclick_show_svg_in_media_library' );

# Формирует данные для отображения SVG как изображения в медиабиблиотеке.
function magniclick_show_svg_in_media_library( $response ) {
  if ( $response['mime'] === 'image/svg+xml' ) {
    // С выводом названия файла
    $response['image'] = [
      'src' => $response['url'],
    ];
  }

  return $response;
}

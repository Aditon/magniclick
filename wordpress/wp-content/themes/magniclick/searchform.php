<form role="search" method="get" id="searchform" action="<?php echo home_url( '/' ) ?>" >
  <input class="search-input search-field top-bar__search-input" type="search" value="<?php echo get_search_query() ?>" name="s" />
  <ul class="codyshop-ajax-search"></ul>
  <span class="icon search-icon js-topBarSearch">
    <input type="submit" id="searchsubmit" value="">
  </span>
</form>
var gulp = require('gulp');
var plumber = require('gulp-plumber');
var notify = require('gulp-notify');
var pug = require('gulp-pug');
var browserSync = require('browser-sync');
var postcss = require('gulp-postcss');
var postcssImport = require('postcss-import');
var postcssCssnext = require('postcss-cssnext');
var less = require('gulp-less');
var mmq = require('gulp-merge-media-queries');
var autoprefixer = require('gulp-autoprefixer');

const del = require('del');

// for image optimization
const image = require('gulp-image');

gulp.task('browser-sync', function(done) {
    browserSync.init({
      proxy: "localhost/magniclick/build",
      // port: 8080
    notify: false
    });
  done();
});


function reload(done){
  browserSync.reload();
  done();
}

gulp.task("less", function(done){
  var processors = [
    postcssImport,
    postcssCssnext
  ];
  return gulp.src([
      'src/css/*.less',
      '!src/css/_*.less',
    ])
    .pipe(mmq({
      log: true
    }))
    .pipe(less())
    .pipe(postcss(processors))
    .on('error', function (err) {
            console.log(err.toString());
            this.emit('end');
        })
    .pipe(autoprefixer({
      overrideBrowserslist: ['last 5 versions'],
      cascade: false
    }))
    .pipe(gulp.dest('./build/css'))
  done();
});

// gulp.task('css', function(done) {
//   gulp.src('src/css/*.css')
//     .pipe(gulp.dest('build/css/'));
//   done();
// });

gulp.task('pug', function(done) {
  return gulp.src(['src/*.pug', '!src/_*.pug'])
    .pipe(plumber())
    .pipe(pug({
      pretty: true
    }))
    .on("error", notify.onError(function(error){
        return "Message to the notifier: " + error.message;
    }))
    .pipe(gulp.dest('build'));
  done();
});

gulp.task('fonts', function(done) {
  gulp.src('src/fonts/**/*')
    .pipe(gulp.dest('build/fonts/'));
  done();
});

gulp.task('img',function(done){
  return gulp.src([
      'src/img/**/*', 'src/*.ico'],  
      {base: 'src/'})
  .pipe(gulp.dest('./build/'));
});

gulp.task('js', function(done) {
  gulp.src(['src/js/*.js', 'src/js/**/*.js'])
    .pipe(gulp.dest('build/js/'));
  done();
});

gulp.task('watch', function(done) {
  gulp.watch(['src/*.pug','src/**/*.pug'], gulp.series('pug', reload));
  gulp.watch(['src/css/*.less', 'src/css/**/*.less'], gulp.series('less', reload));
  gulp.watch(['src/img/*', 'src/*.ico'], gulp.series('img', reload));
  gulp.watch('src/fonts/*', gulp.series('fonts', reload));
  gulp.watch('src/js/*.js', gulp.series('js', reload));
  done();
});

gulp.task('imagemin', function(done) {
  gulp.src(['src/img/*', 'src/img/**/*'])
    .pipe(image())
    .pipe(gulp.dest('build/img'))
  done();
});


gulp.task('del', function(done) {
  (async () => {
    const deletedPaths = await del(['build/**', '!build']);
     console.log('Deleted files and folders:\n', deletedPaths.join('\n'));
})();
  done();
});



gulp.task('default', gulp.series('img', 'fonts', 'less', 'js', 'pug', 'browser-sync', 'watch'));
gulp.task('build', gulp.series('img', 'fonts', 'less', 'js', 'pug', 'imagemin'));
